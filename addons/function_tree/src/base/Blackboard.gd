#==================================================
#	Blackboard
#==================================================
#  存储整个树的全局数据内容
#==================================================
# @datetime: 2021-12-11 22:17:40
#==================================================
extends Reference


var root 
var property := Property.new()


## 属性
class Property:
	var velocity : Vector2
	var moved_velocity : Vector2
	var last_velocity : Vector2
	
	var face_dir : Vector2
	var rotation_speed : float = 0.0
	var climb_speed : float = 0.0
	var move_speed : float = 0.0
	var jump_height : float = 0.0
	var gravity : float = 0.0	# 速度值 [0, 1] 区间
	var max_gravity : float = 0.0	# 最大值

## 可用性
class Enabled:
	var move := __EnabledProperty.new()
	var jump := __EnabledProperty.new()
	var gravity := __EnabledProperty.new()
	var rotation := __EnabledProperty.new()
	var climb := __EnabledProperty.new()
	
	var cast := __EnabledProperty.new()
	var casting := __EnabledProperty.new()

# 自定义数据
var CustomData := {}



#==================================================
#   注册节点
#==================================================
# [ Layer ]
var __layer = {}
func register_layer(node_name: String, node: Node) -> void:
	__layer[node_name] = node

func get_layer(node_name: String) -> Node:
	return __layer[node_name]

func get_layers() -> Array:
	return __layer.values()

func has_layer(node_name: String) -> bool:
	return __layer.has(node_name)


# [ Function ]
var __function_nodes := {}
func register_function(node_name: String, node: Node) -> void:
	if (__function_nodes.has(node_name)
		&& __function_nodes[node_name] != node
	):
		printerr(
			root.host, " "
			, node_name, " "
			, node, " "
			, "已存在相同的名称=", __function_nodes[node_name], " "
			, "已将上次记录的对应名称的节点信息覆盖"
		)
	__function_nodes[node_name] = node

func get_functions() -> Array:
	return __function_nodes.values()

func get_function(node_name: String) -> Node:
	if __function_nodes.has(node_name):
		return __function_nodes[node_name]
	else:
		return null

func has_function(node_name: String) -> bool:
	return __function_nodes.has(node_name)


#==================================================
#   自定义方法
#==================================================
func init_blackboard(root):
	self.root = root
	# 根据宿主的节点类型，设置属性（暂时未启用）
#	if root.host is Node2D:
#		property.velocity = Vector2(0, 0)
#	elif root.host is Spatial:
#		property.velocity = Vector3(0, 0, 0)
	return self



#==================================================
#    属性类
#==================================================
# Enabled 属性
class __EnabledProperty:
	var __value = 0
	
	func value(value=null):
		if value:
			__value += (1 if value else -1)
		else:
			return __value > -1
	
	func get_value():
		return __value > -1
	
	func set_value(value: bool):
		__value += (1 if value else -1)
