#==================================================
#	Layer
#==================================================
#  节点层
#==================================================
# @path: res://addons/function_tree/src/base/layer/Layer.gd
# @datetime: 2021-12-11 21:11:22
#==================================================
extends "../Base.gd"



##  执行功能
## @function  功能名称
## @data  执行时传入的数据
func control(function: String, data):
	get_function(function).control(data)


##  注册到层中
## @node_name  
func register_to_layer(node_name: String):
	blackboard.register_layer(node_name, self)

