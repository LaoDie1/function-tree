#==================================================
#	Skills
#==================================================
#  技能层
#==================================================
# @path: res://addons/function_tree/src/base/layer/standard/Skills.gd
# @datetime: 2021-12-17 15:05:12
#==================================================

extends "../Layer.gd"


#(override)
func _init_data():
	._init_data()
	register_to_layer("Skills")


##  停止所有技能
func stop_all():
	for child in get_children():
		if child.has_method("stop"):
			child.stop()
		else:
			printerr(child, "这个节点不属于这一层！")

