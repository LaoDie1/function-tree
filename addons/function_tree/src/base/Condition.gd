#==================================================
#	Condition - 条件节点
#==================================================
# * 部分的节点的 control 方法中是不经过 is_enabled 
#    法进行验证的，这种节点的条件添加此节点是不会生
#    效的。
#==================================================
# @datetime: 2021-12-22 23:30:19
#==================================================
extends "../Core.gd"



#(override)
func _init_data():
	._init_data()
	get_parent().__condition.push_back(self)


##  条件 
## @return  
func condition() -> bool:
	return true

