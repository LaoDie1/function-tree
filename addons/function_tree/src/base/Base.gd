#==================================================
#	Base
#==================================================
#  功能基类
#==================================================
# @path: res://addons/function_tree/src/base/func/Base.gd
# @datetime: 2021-12-11 19:53:00
#==================================================

extends "../Core.gd"


const FuncRoot = preload("Root.gd")
const Blackboard = preload("Blackboard.gd")



#==================================================
#   Set/Get
#==================================================
func get_root() -> FuncRoot:
	return root

func get_host():
	return host

func get_blackboard() -> Blackboard:
	return blackboard

func get_layer(layer_name: String):
	return blackboard.get_layer(layer_name)

func get_function(node_name: String):
	return blackboard.get_function(node_name)


