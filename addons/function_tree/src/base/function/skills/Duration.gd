#==================================================
#	Duration.gd
#==================================================
#  持续性技能基类
# * 扩展脚本时重写此脚本的 _process_duration 方法进行
#    添加持续性的功能
#==================================================
# @datetime: 2021-12-12 11:51:53
#==================================================
extends "Skill.gd"


## 开始执行持续技能
signal execute_duration
## 持续技能结束
signal duration_finished


## 技能持续时间
export var duration : float = 1.0 setget set_duration


## 开始持续技能
var _executing : bool = false
## 持续时间计时器
var _duration_timer : Timer = Timer.new()



#==================================================
#   Set/Get
#==================================================
#(override)
func is_enabled() -> bool:
	return (.is_enabled()
		&& !_executing
	)

##  设置持续时间
## @value  
func set_duration(value: float) -> void:
	duration = max(0.001, value)
	_duration_timer.wait_time = duration

## 是否正在执行
func is_executing():
	return _executing



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_node():
	._init_node()
	# 持续时间
	_duration_timer = Timer.new()
	_duration_timer.wait_time = duration
	_duration_timer.one_shot = true
	_duration_timer.autostart = false
	_duration_timer.connect("timeout", self, "_duration_finished")
	self.add_child(_duration_timer)


#(override)
func _process_execute(delta):
	._process_execute(delta)
	if _executing:
		_process_duration(delta)



#==================================================
#   Skill 方法
#==================================================
#(override)
func _cast():
	._cast()
	_duration_timer.stop()
	_duration_timer.start(duration)
	_executing = true
	# 持续技能
	emit_signal("execute_duration")

#(override)
func _executed():
	# 这个是在技能发出瞬间后进行处理的
	# 持续性施放之后的处理在 _duration_finished() 里进行
	pass

#(override)
func stop() -> bool:
	if .stop():
		_duration_timer.stop()
		_executing = false
		return true
	return false


##  中断，结束持续（提前结束持续技能）
func interrupt():
	_duration_finished()


##  持续线程 
## @delta  帧时间
func _process_duration(_delta: float) -> void:
	pass


## 持续技能结束
func _duration_finished() -> void:
	_duration_timer.stop()
	_executing = false
	emit_signal("duration_finished")
	if _refresh_timer.is_stopped():
		_refresh_timer.start()
	._executed()


