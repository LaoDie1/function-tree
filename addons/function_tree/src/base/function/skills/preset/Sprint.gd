#==================================================
#	Sprint
#==================================================
#  冲刺
#==================================================
# @datetime: 2021-12-14 16:01:53
#==================================================
extends "res://addons/function_tree/src/base/function/skills/Duration.gd"


## 冲刺速度
export var speed : float = 1000
export var up_direction : Vector2 = Vector2.UP


var velocity : Vector2


#(override)
func is_enabled() -> bool:
	return .is_enabled() && not is_executing()


#(override)
func control(pos: Vector2):
	if .control(pos):
		velocity = get_host() \
				.global_position \
				.direction_to(pos) * speed
		return true
	return false


#(override)
func _process_duration(delta: float) -> void:
	host.move_and_slide(velocity, up_direction)



