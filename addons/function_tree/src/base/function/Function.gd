#==================================================
#	Function
#==================================================
#  功能
#==================================================
# @datetime: 2021-12-11 21:12:54
#==================================================

extends "../Base.gd"


const Property = Blackboard.Property


var __condition := []

var property : Property


#==================================================
#   Set/Get
#==================================================

#(override)
func set_blackboard(value : Blackboard) -> void:
	.set_blackboard(value)
	property = value.property

func get_property() -> Property:
	return property

func is_enabled() -> bool:
	return _condition()

##  下方添加执行条件节点控制执行
## @return  
func _condition() -> bool:
	if __condition.size() == 0:
		return true
	else:
		for condition in __condition:
			if !condition.condition():
				return false
		return true



#==================================================
#   自定义方法
#==================================================
##  控制当前的功能
## @data  
func control(data):
	pass



#==================================================
#   功能方法
#==================================================
##  注册到功能中
## （注册后可直接通过 get_function 获取到这个节点）
## @_name  
func register_to_function(_name: String):
	blackboard.register_function(_name, self)



#==================================================
#   功能节点
#==================================================

const Actions = preload("../layer/standard/Actions.gd")
const Skills = preload("../layer/standard/Skills.gd")
const Events = preload("../layer/standard/Events.gd")
const StateMachineRoot = preload("../layer/custom/StateMachineRoot.gd")


func get_actions() -> Actions:
	return blackboard.get_layer("Actions") as Actions

func get_skills() -> Skills:
	return blackboard.get_layer("Skills") as Skills

func get_events() -> Events:
	return blackboard.get_layer("Events") as Events

func get_statemachine_root() -> StateMachineRoot:
	return blackboard.get_layer("StateMachineRoot") as StateMachineRoot
