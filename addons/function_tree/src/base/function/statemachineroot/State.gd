#==================================================
#	State
#==================================================
#  状态节点
# * 扩展这个脚本进行功能编写
# * 重写 enter & exit & state_process方法
# * 调用 state 的 switch_to 方法切换当前 state	
#==================================================
# @datetime: 2021-9-27 22:45:23
#==================================================
extends "../Function.gd"


var _current_name = ""



#==================================================
#   Set/Get
#==================================================
##  获取自定义 States 节点
## @return  
func get_state_machine():
	return get_parent()

##  当前状态是否是这个状态
func current_state_is_self():
	return get_state_machine().get_current_state() == self


#==================================================
#   状态方法
#==================================================
##  启动 State
## @data  传入数据
func enter(data):
	pass


##  退出 State
func exit():
	pass


## 状态执行线程（只执行当前 state 的 state_process）
func state_process(_delta: float) -> void:
	pass


##  切换到目标 State
## @state_name  State 名称
## @data=null  传入数据
func switch_to(state_name: String, data = null) -> bool:
	return get_state_machine().switch_to(state_name, data)


