#==================================================
#	By Move Event
#==================================================
# 根据移动事件进行操控
# * 重写此脚本的动作方法进行控制
# * 使用此节点执行动作时，注意切换时当前动作与上次动作
#    是否连贯，然后需要作一些条件判断
#==================================================
# @datetime: 2021-12-15 17:35:02
#==================================================
extends "Event.gd"


var _move


#==================================================
#   自定义方法
#==================================================
#(override)
func _init_finish():
	._init_finish()
	_move = get_function("PlatformMove")
	_move.connect("start_move", self, "_start_move")
	_move.connect("stop_move", self, "_stop_move")
	_move.connect("fly_up", self, "_fly_up")
	_move.connect("fall_down", self, "_fall_down")
	_move.connect("fall_on_floor", self, "_fall_on_floor")
	_move.connect("touch_ceil", self, "_touch_ceil")
	_move.connect("hit_wall", self, "_hit_wall")
	_move.connect("get_off_wall", self, "_get_off_wall")


#(override)
func _process_finish(arg0):
	_moved(get_property().moved_velocity)



#==================================================
#   动作信号方法（重写下面方法进行播放动画）
#==================================================
##  正在移动 
## @velocity  移动的向量
func _moved(velocity: Vector2) -> void:
	pass

##  开始移动时
func _start_move() -> void:
	pass

##  停止移动时
func _stop_move() -> void:
	pass

##  开始向下落时
func _fall_down() -> void:
	pass

##  落在地板上时
func _fall_on_floor() -> void:
	pass

##  跳跃飞起时
func _fly_up() -> void:
	pass

##  碰到天花板时
func _touch_ceil():
	pass

## 碰到墙壁
func _hit_wall():
	pass

## 离开墙壁
func _get_off_wall():
	pass
