#==================================================
#	Move Signal
#==================================================
#  根据移动的时机发出信号
#==================================================
# @datetime: 2021-12-15 11:40:14
#==================================================

extends "Event.gd"


signal start_move	# 开始移动
signal stop_move	# 停止移动
signal fly_up		# 飞起
signal fall_down	# 落下（从顶点开始下落）
signal fall_on_floor	# 落在地板上
signal touch_ceil	# 碰到天花板
signal hit_wall		# 碰到墙壁
signal get_off_wall	# 离开墙壁


# 上一次状态是否是移动
var _last_move_state : bool = false
# 上一次状态是否是在地板上
var _on_floor : bool = true
# 上次状态是否碰到了墙壁
var _on_wall : bool = false
# 是否向上升的状态
var _is_up : bool = false



#==================================================
#   Set/Get
#==================================================
func is_fall() -> bool:
	return (get_property().velocity.y > 0 
		&& not host.is_on_floor() 
	)

func is_fly() -> bool:
	return (get_property().velocity.y < 0 
		&& not host.is_on_floor()
	)



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_data():
	._init_data()
	register_to_function("PlatformMove")


#(override)
func _process_finish(arg0):
	# [ 根据上次移动的 x 值，判断是否移动了位置 ]
	# 开始移动了
	if (
		get_property().moved_velocity.x != 0
		&& _last_move_state == false
	):
		_last_move_state = true
		emit_signal("start_move")
	
	# 停止移动了
	elif (
		get_property().moved_velocity.x == 0 
		&& _last_move_state
	):
		_last_move_state = false
		emit_signal("stop_move")
	
	# 上次在地板上，这此不在地板上
	if (_on_floor 
		&& !host.is_on_floor()
		&& property.moved_velocity.y != 0
	):
		if property.moved_velocity.y <= 0:
			_fly()
		else:
			_fall()
	
	# 上次不在地板上，这次在地板上
	elif (!_on_floor 
		&& host.is_on_floor() 
		&& property.moved_velocity.y == 0
	):
		_fall_on_floor()
	
	# 如果不在地板上
	# 且上次是上升的
	# 现在 y 是下降的时
	if (!_on_floor
		&& _is_up
		&& property.moved_velocity.y > 0
	):
		_fall()
	
	# 墙壁
	elif (_on_wall 
		&& !host.is_on_wall()
	):
		_on_wall = false
		emit_signal("get_off_wall")
	
	elif (!_on_wall 
		&& host.is_on_wall()
	):
		_on_wall = true
		emit_signal("hit_wall")
	
	# 碰到天花板
	elif host.is_on_ceiling():
		emit_signal("touch_ceil")



#==================================================
#   在平台上的
#==================================================
##  落在地板上
func _fall_on_floor() -> void:
	_on_floor = true
	emit_signal("fall_on_floor")


# 飞起
func _fly() -> void:
	_on_floor = false
	_is_up = true
	emit_signal("fly_up")


# 向下落
func _fall() -> void:
	_on_floor = false
	_is_up = false
	emit_signal("fall_down")


