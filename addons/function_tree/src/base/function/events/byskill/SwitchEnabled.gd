#==================================================
#	Switch Enabled
#==================================================
#  在 Skill 发出信号后，切换 Enable 状态
#==================================================
# @datetime: 2021-12-15 13:52:34
#==================================================

tool
extends "BySkill.gd"


const PROP_SET_ENABLED = "_set_enabled"
const PROP_TO_VALUE = "_to_value"



#(override)
func __editor_get_custom_prop_data() -> Array:
	var list = .__editor_get_custom_prop_data()
	
	# Enabled 列表
	var enabled_list := ""
	var temp = Blackboard.Enabled.new()
	var s_prop_list = temp.get_script().get_script_property_list()
	for i in s_prop_list:
		enabled_list += i['name'] + ","
	enabled_list = enabled_list.trim_suffix(",")
	__prop_list.push_back({
		name = PROP_SET_ENABLED,
		type = TYPE_STRING,
		hint = PROPERTY_HINT_ENUM,
		hint_string = enabled_list
	})
	# 缺省值
	if !get(PROP_SET_ENABLED):
		set(PROP_SET_ENABLED, s_prop_list[0]['name'])
	
	# 转换到的值
	__prop_list.push_back({name=PROP_TO_VALUE, type=TYPE_BOOL})
	# 缺省值
	if !get(PROP_TO_VALUE):
		set(PROP_TO_VALUE, false)
	
	return list


##(override)
func control(value: bool):
	pass


#(override)
func trigger():
	print('---')
	control(get(PROP_TO_VALUE))


