#==================================================
#	Play Animation
#==================================================
#  播放动画
#==================================================
# @datetime: 2021-12-15 17:30:59
#==================================================
tool
extends "BySkill.gd"


signal play(anim_name)


## 等待时间之后播放
##（一般等待零点几秒时间最好，会避免一些播放冲突的问题）
var wait_time: float = 0.05
## 要播放的动画
var play_animation : String = ""


#(override)
func __editor_get_custom_prop_data() -> Array:
	var list : Array = []
	list.push_back( {
		name = 'wait_time',
		type = TYPE_REAL,
	})
	list.push_back({
		name = "play_animation",
		type = TYPE_STRING,
	})
	return list


#(override)
func trigger():
	if wait_time > 0:
		yield(get_tree().create_timer(wait_time), "timeout")
	
	if play_animation:
		emit_signal("play", play_animation)
	else:
		printerr(self, "没有设置动画名！")


