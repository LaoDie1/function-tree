#==================================================
#	By Skill
#==================================================
#  根据 Skill 信号触发功能
#==================================================
# @path: res://addons/function_tree/src/base/function/events/byskill/BySkill.gd
# @datetime: 2021-12-16 20:00:21
#==================================================

tool
extends "../Event.gd"


signal triggered

const NONE_SKILL = "[None]"

const PROP_SELECT_SKILL = "_select_skill"
const PROP_CONNECT_SIGNAL = "_connect_signal"


var __prop_list : Array = []
var __prop_data : Dictionary = {}
var __has_skill : bool = true



#==================================================
#   内部方法
#==================================================
func _get_property_list():
	__prop_list = []
	# 技能列表
	var _r = __editor_get_root()
	if _r:
		# 选择技能
		var skills = __editor_find_skills(_r)
		if skills && skills.get_child_count() > 0:
			var select_skill = ""
			for child in skills.get_children():
				select_skill += child.name + ","
			__prop_list.push_back({
				name = PROP_SELECT_SKILL,
				type = TYPE_STRING,
				hint = PROPERTY_HINT_ENUM,
				hint_string = select_skill.trim_suffix(","),
			})
			__has_skill = true
		else:
			__has_skill = false
		# 缺省值
		var select_node : Node = null
		if skills:
			var value := ""
			var _p = get(PROP_SELECT_SKILL)
			# 之前没有设置过
			if !_p:
				if skills.get_child_count() > 0:
					# 默认设置第一个
					_p = skills.get_child(0).name
					select_node = skills.get_child(0)
				else:
					_p = NONE_SKILL
				set(PROP_SELECT_SKILL, _p)
				
			# 如果之前已经设置过了则
			else:
				# 如果有这个节点
				if skills.has_node(_p):
					select_node = skills.get_node(_p)
				# 没有这个节点
				else:
					_p = NONE_SKILL
					set(PROP_SELECT_SKILL, _p)
		
		# 连接的信号
		if select_node:
			var signals = select_node.get_script().get_script_signal_list()
			var select_signal := ""
			for i in signals:
				select_signal += i['name']+ ","
			__prop_list.push_back({
				name = PROP_CONNECT_SIGNAL,
				type = TYPE_STRING,
				hint = PROPERTY_HINT_ENUM,
				hint_string = select_signal.trim_suffix(","),
			})
			# 缺省值
			if !get(PROP_CONNECT_SIGNAL):
				set(PROP_CONNECT_SIGNAL, signals[0]['name'])
		else:
			set(PROP_CONNECT_SIGNAL, "")
		
	# 添加其他自定义属性
	for i in __editor_get_custom_prop_data():
		__prop_list.push_back(i)
	return __prop_list

func _set(property, value):
	__prop_data[property] = value

func _get(property):
	if __prop_data.has(property):
		return __prop_data[property]

func _get_configuration_warning():
	if __has_skill:
		return ""
	else:
		return "没有 Skill 节点，功能将不生效！"



#==================================================
#   自定义方法（编辑器）
#==================================================
func __editor_get_root():
	if Engine.editor_hint:
		var p = get_parent()
		while p:
			p = p.get_parent()
			if p is FuncRoot:
				return p
			elif p is Viewport:
				return null
		return null
	else:
		return get_root()

func __editor_find_skills(node: Node) -> Node:
	if Engine.editor_hint:
		for child in node.get_children():
			if child is Skills:
				return child
		for child in node.get_children():
			var s = __editor_find_skills(child)
			if s:
				return s
		return null
	else:
		return get_layer("Skills")


func __editor_get_custom_prop_data() -> Array:
	return []



#==================================================
#   自定义方法
#==================================================
##  获取选中的节点
## @return  
func get_select_skill() -> Node:
	var _skill_name = get(PROP_SELECT_SKILL)
	return get_function(_skill_name) as Node


#(override)
func _init_node():
	._init_node()
	var n := get_select_skill() as Node
	if n:
		var _signal = get(PROP_CONNECT_SIGNAL)
		if n.connect(_signal, self, "__trigger__") != OK:
			printerr("连接时出现错误")
	else:
		printerr(name, "没有这个节点！")


func __trigger__():
	trigger()
	emit_signal("triggered")


##  触发信号
func trigger():
	pass

