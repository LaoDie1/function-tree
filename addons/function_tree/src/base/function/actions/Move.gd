#==================================================
#	Move
#==================================================
#  移动
#==================================================
# @path: res://addons/function_tree/src/base/function/actions/Move.gd
# @datetime: 2021-12-11 21:18:18
#==================================================
extends "Action.gd"



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_data():
	._init_data()
	register_to_function("Move")
	assert(get_host() is KinematicBody2D, "Host 节点必须是 KinematicBody2D 类型!")


#(override)
func control(direction):
	if is_enabled():
		property.velocity += direction


#(override)
func _process_execute(delta: float):
	if is_enabled():
		# 移动控制 host
		var pos = host.global_position
		property.last_velocity = property.velocity
		# 移动
		property.velocity *= property.move_speed
		host.move_and_slide(property.velocity)
		# 计算当前位置和移动前的位置的差值，得到移动的向量
		property.moved_velocity = host.global_position - pos


#(override)
func _process_finish(_arg0):
	property.velocity = Vector2(0,0)

