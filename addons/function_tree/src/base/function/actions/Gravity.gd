#==================================================
#	Gravity
#==================================================
#  重力
# 不需要调用 control 方法使用功能
#==================================================
# @path: res://addons/function_tree/src/base/function/actions/Gravity.gd
# @datetime: 2021-12-15 10:21:16
#==================================================
extends "Action.gd"



#==================================================
#   动作方法
#==================================================

#(override)
func _init_data():
	._init_data()
	if !property.velocity:
		property.velocity 


##  重力
## @delta  帧时间
func control(delta):
	printerr("不需要调用这个方法即可使用！")



#==================================================
#   自定义方法
#==================================================
#(override)
func _process_input(delta):
	if is_enabled():
		# 下坠
		property.velocity.y = lerp(
			property.velocity.y, 
			property.max_gravity,
			property.gravity * delta
		)
		property.velocity.y += property.velocity.y * delta


#(override)
func _process_execute(arg0):
	property.moved_velocity.y = get_host() \
		.move_and_slide(
			Vector2(0, property.velocity.y), 
			Vector2.UP
		).y


#(override)
func _process_finish(arg0):
	if get_host().is_on_floor():
		property.velocity.y = 0


