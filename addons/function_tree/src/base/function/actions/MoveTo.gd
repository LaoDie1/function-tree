#==================================================
#	Move To
#==================================================
#  移动到目标位置
#==================================================
# @datetime: 2021-12-29 18:15:45
#==================================================
extends "Action.gd"


signal arrived

## 到达距离（在这个距离内，则视为到达）
export var arrive_distance : float = 10 setget set_arrive_distance


var target_pos : Vector2
var running : bool = false
var squared_arrive_distance : float



#==================================================
#   Set/Get
#==================================================
func set_arrive_distance(value: float) -> void:
	arrive_distance = value
	squared_arrive_distance = pow(arrive_distance, 2)



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_data():
	._init_data()
	register_to_function("MoveTo")
	set_arrive_distance(arrive_distance)


#(override)
func control(pos: Vector2):
	target_pos = pos
	running = true


#(override)
func _process_execute(_arg0):
	if running:
		var dir = host.global_position.direction_to(target_pos)
		get_function("Move").control(dir)
		if (host \
			.global_position \
			.distance_squared_to(target_pos) <= squared_arrive_distance
		):
			running = false
			emit_signal("arrived")

##  停止
func stop():
	running = false
