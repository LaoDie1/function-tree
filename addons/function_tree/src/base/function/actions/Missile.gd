#==================================================
#	Missile
#==================================================
#  导弹
#==================================================
# @link: http://kidscancode.org/godot_recipes/ai/homing_missile/
# @path: res://addons/function_tree/src/senior/Missile.gd
# @datetime: 2021-12-14 15:41:39
#==================================================

extends "Action.gd"


export var speed = 350
export var steer_force = 50.0


var velocity = Vector2.ZERO
var acceleration = Vector2.ZERO
var target : Node2D = null

var steer : Vector2 = Vector2.ZERO
var running : bool = false


#(override)
func _process_execute(delta):
	if running:
		acceleration += _seek()
		velocity += acceleration * delta
		velocity = velocity.clamped(speed)
		host.rotation = velocity.angle()
		host.position += velocity * delta


#(override)
##  控制
## @_target  追击目标
func control(_target: Node2D):
	if _target && .control(_target):
		running = true
		host.rotation += rand_range(-0.09, 0.09)
		velocity = host.transform.x * speed
		target = _target
		return true
	return false


#(override)
func stop() -> bool:
	if .stop():
		running = false
		return true
	return false


func _seek():
	if target:
		var desired = (target.position - host.position).normalized() * speed
		steer = (desired - velocity).normalized() * steer_force
	return steer



