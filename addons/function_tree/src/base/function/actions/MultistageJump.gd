#==================================================
#	Multistage Jump
#==================================================
#  多级跳跃
#==================================================
# @path: res://addons/function_tree/src/base/function/actions/MultistageJump.gd
# @datetime: 2021-12-15 10:28:36
#==================================================

extends "res://addons/function_tree/src/base/function/Function.gd"


## 已达最大跳跃次数
signal maximum_jump_reached


## 多段跳跃高度
##（数量大于 0 可以连续跳跃）
##（每阶跳跃的高度，根据此属性的值索引）
export var multistage_jump_height : PoolIntArray
## 跳跃间隔时间
export var jump_interval : float = 0.3
## 当下落时才能跳跃
export var when_falling : bool = true
## 当前跳跃速度是否可以叠加
export var superposition : bool = true


## 多阶跳跃的间隔计时器
var _jump_interval_timer : Timer = Timer.new()
## 跳跃次数
var _jump_count : int = -1



#==================================================
#   Set/Get
#==================================================
#(override)
func is_enabled() -> bool:
	return (
		.is_enabled() 
		&& _jump_interval_timer.time_left <= 0
	)



#==================================================
#   内置方法
#==================================================

#(override)
func _init_data():
	._init_data()
	register_to_function("MultistageJump")


#(override)
func _init_node():
	._init_node()
	# 添加多阶跳跃计时器
	_jump_interval_timer.one_shot = true
	_jump_interval_timer.autostart = false
	_jump_interval_timer.wait_time = jump_interval
	add_child(_jump_interval_timer)


#(override)
func control(arg0):
	if host.is_on_floor():
		_jump_count = -1
	
	if is_enabled():
		# 当下落时才能跳跃，但当前为向上移动状态时不继续执行
		if when_falling && get_property().velocity.y < 0:
			return
		
		# 单次跳跃
		if _jump_count == -1:
			if host.is_on_floor():
				if arg0:
					_jump(arg0)
				else:
					_jump(get_property().jump_height)
		
		# 开始多阶跳跃
		elif _jump_count < multistage_jump_height.size():
			var height = multistage_jump_height[_jump_count]
			_jump(height)


##  跳跃
## @height  跳跃高度
func _jump(height: float) -> void:
	# 追加跳跃速度（如果是跳跃状态时，会追加速度）
	if superposition && get_property().velocity.y <= 0:
		height -= get_property().velocity.y
	
	# 执行跳跃动作
	get_actions().control("Jump", height)
	
	# 多阶跳跃间隔计时器
	_jump_count += 1
	_jump_interval_timer.start(jump_interval)
	# 当前跳跃次数已用完
	if _jump_count >= multistage_jump_height.size():
		emit_signal("maximum_jump_reached")


##  刷新跳跃
## @count  设置已跳跃次数
func refresh() -> void:
	_jump_count = -1

