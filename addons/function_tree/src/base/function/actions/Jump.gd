#==================================================
#	Jump
#==================================================
#  跳跃
# * 一般要和 gravity 结合使用
#==================================================
# @datetime: 2021-9-8 12:11:41
#==================================================

extends "res://addons/function_tree/src/base/function/actions/Action.gd"


#(override)
func _init_data():
	._init_data()
	register_to_function("Jump")


#(override)
## @height  跳跃高度
func control(height = INF) -> void:
	if is_enabled():
		# 如果高度是无限大，则跳跃高度默认为属性跳跃高度
		if height == null || height == INF:
			height = get_property().jump_height
		
		# 跳跃
		if get_property().velocity.y > -height:
			get_property().velocity.y = -height
		else:
			get_property().velocity.y += -height



