#==================================================
#	Move Platform
#
#  横版平台移动方式
#==================================================
# @datetime: 2021-10-8 16:24:33
#==================================================
extends "Move.gd"



#==================================================
#   Set/Get
#==================================================
#(override)
func get_host() -> KinematicBody2D:
	return .get_host() as KinematicBody2D



#==================================================
#   自定义方法
#==================================================
#(override)
func control(direction):
	# 因为是在平台上移动，所以只设置 x 轴位置
	if is_enabled():
		get_property().velocity.x = sign(direction.x)


#(override)
func _process_execute(delta: float):
	# 移动控制 host
	var pos = host.global_position
	get_property().last_velocity = get_property().velocity
	if get_property().velocity.x != 0:
		# 移动。在这里是 *= 累积，所以执行结束后要将 x 归零
		get_property().velocity.x *= get_property().move_speed
		get_host().move_and_slide(Vector2(get_property().velocity.x, 0), Vector2.UP)
		# 计算当前位置和移动前的位置的差值，得到移动的向量
		get_property().moved_velocity.x = host.global_position.x - pos.x
		property.face_dir.x = sign(get_property().moved_velocity.x)
	else:
		get_property().velocity.x = 0
		get_property().moved_velocity.x = 0


#(override)
func _process_finish(arg0):
	get_property().velocity.x = 0
