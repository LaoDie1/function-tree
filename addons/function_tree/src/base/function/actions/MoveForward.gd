#==================================================
#	Move Forward
#==================================================
#  向前移动
# * 总是向面向的方向进行移动
#==================================================
# @path: res://test/bullet/MoveForward.gd
# @datetime: 2021-12-16 08:12:19
#==================================================
extends "res://addons/function_tree/src/base/function/Function.gd"


export (float, 0, 360.0) var offset_angle : float = 0

var last_rot : float
var velocity : Vector2
var move


#(override)
func _init_data():
	._init_data()
	if !property.move_speed:
		property.move_speed = 0


#(override)
func _init_node():
	._init_node()
	move = get_function("Move")
	update_velocity()


#(override)
func _process_input(arg0):
	if last_rot != host.global_rotation:
		update_velocity()
	move.control(velocity)


##  更新
func update_velocity():
	last_rot = host.global_rotation + deg2rad(offset_angle)
	velocity = Vector2.LEFT.rotated(last_rot)




