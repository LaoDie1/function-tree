#==================================================
#	Sm Animation
#==================================================
#  选取一个 AnimationTree 进行控制播放动画
# * AnimationTree 的 tree_root 属性需要添加 
#    AnimationNodeStateMachine 类型的资源
# * 勾选 auto_connect_animations 自动连接 Animations
#    在 Animations 播放动画时调用此节点 play_anima 
#    方法
#==================================================
# @datetime: 2021-12-15 18:12:15
#==================================================
tool
extends "Animation.gd"


## 自动连接 Animations 节点的播放信号
var auto_connect_animations : bool = true
var animated_sprite : NodePath setget set_animated_sprite


var _anim_sprite : AnimatedSprite



#==================================================
#   Set/Get
#==================================================
func set_animated_sprite(value: NodePath) -> void:
	animated_sprite = value
	update_configuration_warning()



#==================================================
#   内部方法
#==================================================
func _get_property_list():
	return [{
			name = "animated_sprite"
			, type = TYPE_NODE_PATH
			, hint = 35
			, hint_string = "AnimatedSprite"
		} , {
			name = 'auto_connect_animations'
			, type = TYPE_BOOL
		}
	]


func _get_configuration_warning():
	if self.is_inside_tree():
		var node = get_node_or_null(animated_sprite)
		if node == null:
			return "没有设置 AnimatedSprite ！"
		else:
			return ""



#==================================================
#   自定义方法
#==================================================

#(override)
func _init_data():
	._init_data()
	if has_node(animated_sprite):
		_anim_sprite = get_node(animated_sprite) as AnimatedSprite
		if _anim_sprite:
			_anim_sprite.playing = true
			_anim_sprite.animation = "default"
	
	# 连接信号
	if (auto_connect_animations
		&& get_function("Animations")
		&& !get_function("Animations").is_connected("play", self, "play_anima")
	):
		if get_function("Animations").connect("play", self, "play_anima") != OK:
			print_debug("连接时出现了错误")


#(override)
func play_anima(anim_name: String):
	_anim_sprite.play(anim_name)


