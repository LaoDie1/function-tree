#==================================================
#	Animations
#==================================================
#  动画层
#==================================================
# @datetime: 2021-12-20 01:42:33
#==================================================
extends "res://addons/function_tree/src/custom/CustomLayer.gd"


signal play(anim_name)


var _current_animation : String = ""



#==================================================
#   获取当前播放的动画
#==================================================
func get_current_animation() -> String:
	return _current_animation



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_data():
	register_to_layer("Animations")


##  播放动画
## @anim_name  动画名
func play_anima(anim_name: String) -> void:
	_current_animation = anim_name
	emit_signal("play", anim_name)

