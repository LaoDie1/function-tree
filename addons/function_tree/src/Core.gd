#==================================================
#	Core
#==================================================
# 核心
#==================================================
# @path: res://addons/function_tree/src/base/Core.gd
# @datetime: 2021-12-11 20:02:21
#==================================================

extends Node


var root setget set_root, get_root 
var host setget set_host, get_host
var blackboard setget set_blackboard, get_blackboard



#==================================================
#   Set/Get
#==================================================
func set_root(value) -> void:
	root = value

func get_root():
	return root

func set_host(value) -> void:
	host = value

func get_host():
	return host

func set_blackboard(value) -> void:
	blackboard = value

func get_blackboard():
	return blackboard

func get_custom_data() -> Dictionary:
	return blackboard.CustomData

##  是否是 Function 节点 
## @node  
## @return  
func is_function_node(node: Node) -> bool:
	return node.has_method("_process_input")



#==================================================
#   内部方法
#==================================================
#func _enter_tree():
#	for child in get_children():
#		if is_function_node(child):
#			__children__.push_back(child)



#==================================================
#   自定义方法
#==================================================
## 初始化数据
func _init_data():
	pass

## 初始化节点
func _init_node():
	pass

## 初始化完成
func _init_finish():
	pass

##  接收输入
## （在这里进行对数据的处理，比如调用控制移动
## 因为调用的时候并不进行角色的控制，只是将数据
## 增加上去了）
func _process_input(delta):
	pass

##  执行功能
## （在这里对节点进行控制，设置节点的位置，属性等操作）
func _process_execute(delta):
	pass

##  前几个线程都完成
## （在这里对所有内容进行处理）
func _process_finish(delta):
	pass


