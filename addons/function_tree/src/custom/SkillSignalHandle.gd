#==================================================
#	Skill Signal Handle
#==================================================
#  技能信号操作
# * 根据节点发出的信号对各种信息进行处理
# * 重写 handle 方法进行操作数据
#==================================================
# @datetime: 2021-12-22 18:52:45
#==================================================
extends "res://addons/function_tree/src/custom/CustomFunction.gd"


const SIGNAL_LIST = [
		"ready_execute",
		"executed",
		"execute_finished",
		"status_refreshed",
		"stopped",
		"execute_duration",
		"duration_finished",
	]



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_node():
	._init_node()
	if get_blackboard().has_layer("Skills"):
		for skill in get_skills().get_children():
			connect_skill_signal(skill)


##  连接技能信号
## @skill  
func connect_skill_signal(skill: Node) -> void:
	for _signal in SIGNAL_LIST:
		if (skill.has_signal(_signal)
			&& skill.connect(_signal, self, "handle", [skill, _signal]) != OK
		):
			print_debug(skill, "技能连接信号时出现错误")


# [ 重写下面方法进行修改对数据的处理方式 ]
##  处理数据 
## @skill  技能节点
## @signal_name  发出的信号名称
## @data  对应技能的数据
func handle(skill: Node, signal_name: String):
	printerr(name, "重写这个方法对信号进行处理！")
	pass


