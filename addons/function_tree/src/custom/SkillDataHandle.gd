#==================================================
#	Skill Signal Handle
#==================================================
#  技能信号操作
# * 一定要设置 Data 属性添加设置数据
# * 根据节点发出的信号对各种信息进行处理
# * 重写 handle 方法进行操作数据
# * 对技能信号施放后的操作
#==================================================
# @datetime: 2021-12-22 18:52:45
#==================================================
extends "res://addons/function_tree/src/custom/CustomFunction.gd"


const SIGNAL_LIST = [
		"ready_execute",
		"executed",
		"execute_finished",
		"status_refreshed",
		"stopped",
		"execute_duration",
		"duration_finished",
	]

# 信号的数据，数据格式如下
"""
Data = {
	信号名 = {
		# 添加到的技能节点名则会生效，不添加则不生效
		节点名 = {
			字典数据
			...
		},
		...
	},
	...
}
"""
var Data : SignalData = SignalData.new()



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_node():
	._init_node()
	if get_blackboard().has_layer("Skills"):
		for skill in get_skills().get_children():
			connect_skill_signal(skill)


##  连接技能信号
## @skill  
func connect_skill_signal(skill: Node) -> void:
	for _signal in SIGNAL_LIST:
		if (skill.has_signal(_signal)
			&& skill.connect(_signal, self, "__skill_signal_data__", [skill, _signal]) != OK
		):
			print_debug(skill, "技能连接信号时出现错误")


##  技能信号
## @skill  技能节点
## @signal_name  信号名称
func __skill_signal_data__(skill: Node, signal_name: String) -> void:
	var data = Data[signal_name]
	if data.has(skill.name):
		handle(skill, signal_name, data[skill.name])


# [ 重写下面方法进行修改对数据的处理方式 ]
##  处理数据 
## @skill  技能节点
## @signal_name  发出的信号名称
## @data  对应技能的数据
func handle(skill: Node, signal_name: String, data):
	printerr(name, "重写这个方法对数据进行处理！")
	pass



#==================================================
#   信号数据
#==================================================
class SignalData:
	
	# 下面变量名要和技能的信号名一样
	var ready_execute := {} 
	var executed := {}  
	var execute_finished := {} 
	var status_refreshed := {} 
	var stopped := {} 
	var execute_duration := {} 
	var duration_finished := {} 
	
	static func keys():
		return [
			"ready_execute",
			"executed",
			"execute_finished",
			"status_refreshed",
			"stopped",
			"execute_duration",
			"duration_finished",
		]


