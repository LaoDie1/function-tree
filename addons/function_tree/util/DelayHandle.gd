#==================================================
#	Delay Handle
#==================================================
#  延迟操作
# * 因为 EditorPlugin 插件中的 Handle 方法会在选中节
#    点后多次调用，所以在这里进行延迟一帧再调用 edit
#    和 make_visible 方法
#==================================================
# @datetime: 2021-12-16 18:07:25
#==================================================

extends Reference


const FuncCore = preload("res://addons/function_tree/src/Core.gd")

var host : EditorPlugin
var tag := true



func _init(host: EditorPlugin):
	self.host = host


func handles(object):
	if tag:
		tag = false
		yield(host.get_tree(), "physics_frame")
		tag = true
		if host.has_method("make_visible"):
			host.make_visible(object is FuncCore)
		if host.has_method("edit"):
			host.edit(object)

