extends Reference


const FileUtil = preload("FileUtil.gd")

var _dir = Directory.new()
var _exclude_ : Dictionary = {}


##  添加排除的 Key
## @data  
func add_exclude(data):
	if data is Array:
		for i in data:
			_exclude_[i] = true
	else:
		_exclude_[data] = true
	return self


##  生成
func generate(scan_path: String) -> Script:
	if not _dir.dir_exists(scan_path):
		print_debug("要扫描的文件路径不存在！")
		return null
	# 扫描文件
	var list = FileUtil.scan(scan_path, true).keys()
	# 开始生成
	if list.size() > 0:
		var paths = []
		for p in list:
			paths.push_back(scan_path.plus_file(p))
		var code = _format(_generate_code(paths))
		var script = GDScript.new()
		script.source_code = code
		return script
	return null


func _format(code: String) -> String:
	return """
############################################
#  [ 注意 ] 
# 这个文件是动态生成的，不要修改这个文件
# 否则手动修改的内容将不复存在
############################################

extends Reference

%s

""" % code


##  生成脚本代码
## @paths  脚本路径列表
func _generate_code(paths: Array):
	var code = ""
	for path in paths:
		var name = path.get_basename().get_file().capitalize().replace(" ", "")
		if _exclude_.has(name):
			continue
		code += 'const {name} = preload("{path}") \n'.format({
			name = name,
			path = path,
		})
	return code


