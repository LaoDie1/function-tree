extends Reference


##  获取文件列表
## @scan_path  扫描的路径
## @recursion  递归扫描（全部子路径都扫描）
static func scan(scan_path: String, recursion: bool = true) -> Dictionary:
	if !scan_path:
		return {}
	var dir = Directory.new()
	if !dir.dir_exists(scan_path):
#		printerr(scan_path, "目录不存在！")
		return {}
	
	dir.open(scan_path)
	dir.list_dir_begin()
	
	# 去掉 . 和 .. 两个文件夹目录
	dir.get_next()
	dir.get_next()
	
	# 遍历文件夹下所有的文件
	var data = {}
	var file = dir.get_next()
	while file:
		# 文件夹
		if dir.current_is_dir():
			if recursion:
				data[file] = scan(scan_path.plus_file(file), recursion)
		# 文件
		else:
			# GD文件则添加
			if file.get_extension() == 'gd':
				data[file] = true
		file = dir.get_next()
	dir.list_dir_end()
	return data


##  文件内容是否相同 
## @path  文件路径
## @content  内容
static func equals_file_content(path: String, content: String):
	var file = File.new()
	if (file.file_exists(path)
		&& file.open(path, File.READ) == OK
	):
		var text = file.get_as_text()
		file.close()
		return content == text
	return false


##  保存
## @interface  
## @path  
## @resource  
static func save(path: String, resource: GDScript):
	if resource:
		var file = File.new()
		if file.open(path, File.WRITE) == OK:
			file.store_string(resource.source_code)
			file.close()
			pass
	#			print_debug("保存完成")
		else:
			print_debug("保存时出现错误")

