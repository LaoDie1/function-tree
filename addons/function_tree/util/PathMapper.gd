extends Reference


const LayerPath = "res://addons/function_tree/src/base/layer/"
const FunctionPath = "res://addons/function_tree/src/base/function/"
const CustomPath = "res://addons/function_tree/src/custom/"

const Root = preload("res://addons/function_tree/src/base/Root.gd")
const Category = preload("res://addons/function_tree/src/base/category/Category.gd")
const Standard = preload("res://addons/function_tree/src/base/category/Standard.gd")
const Custom = preload("res://addons/function_tree/src/base/category/Custom.gd")
const Layer = preload("res://addons/function_tree/src/base/layer/Layer.gd")
const Function = preload("res://addons/function_tree/src/base/function/Function.gd")

const CustomLayer = preload("res://addons/function_tree/src/custom/CustomLayer.gd")
const CustomFunction = preload("res://addons/function_tree/src/custom/CustomFunction.gd")


var dir = Directory.new()


#==================================================
#   Set/Get
#==================================================
static func is_root(node: Node) -> bool:
	return node is Root

static func is_standard(node: Node) -> bool:
	return node is Standard

static func is_custom(node: Node) -> bool:
	return node is Custom

static func is_layer(node: Node) -> bool:
	return node is Layer

static func is_function(node: Node) -> bool:
	return node is Function

static func is_custom_layer(node: Node) -> bool:
	return node is CustomLayer

static func is_custom_function(node: Node) -> bool:
	return node is CustomFunction



#==================================================
#   自定义方法
#==================================================
##  对应脚本映射到的路径
## @script_path  脚本路径
## @return  
func map(script_path: String) -> String:
	var path = script_path.get_basename().get_file().to_lower().replace(" ", "")
	return FunctionPath.plus_file(path)
