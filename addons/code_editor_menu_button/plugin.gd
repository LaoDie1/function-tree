#==================================================
#	Plugin
#==================================================
# 代码编辑器菜单按钮插件
#==================================================
# @path: res://addons/code_editor_menu_button/plugin.gd
# @datetime: 2021-6-23 22:26:11
#==================================================
tool
extends EditorPlugin


const ClassManager = preload("class_manager.gd")


# 插件类
const AddComments = preload("plugin/comments/plugin.gd")
const OverrideFunc = preload("plugin/override/plugin.gd")
const SetgetDialog = preload("plugin/setget/plugin.gd")

var class_manager = ClassManager.new(get_editor_interface())
var menu_button = ClassManager.NodeMenuButtonPlus.new(get_button_name())

# 插件
var add_comments = AddComments.new(get_editor_interface())
var override_func = OverrideFunc.new(get_editor_interface())
var setget_dialog = SetgetDialog.new(get_editor_interface())



#==================================================
#   Set/Get
#==================================================
func get_button_name():
	if OS.get_locale_language() == 'zh':
		return "代码"
	else:
		return "Source"



#==================================================
#   内置方法
#==================================================
func _enter_tree():
	yield(get_tree().create_timer(0.1), "timeout")
	init_menu_button()


func _exit_tree():
	_remove_menu_button()



#==================================================
#   自定义方法
#==================================================
## 初始化菜单按钮
func init_menu_button():
	# 添加菜单按钮到编辑器当中
	menu_button.name = get_button_name() 
	class_manager.util_script_editor.add_menu_button(menu_button)
	
	# 初始化 MenuButton 按钮
	add_comments.init_menu_button(menu_button)
	
	menu_button.add_separator()
	override_func.init_menu_button(menu_button)
	
	setget_dialog.init_menu_button(menu_button)
	
	# 添加关闭插件菜单项
	menu_button.add_separator()
	menu_button.add_popup_item("重新加载插件", self, "reload_plugin")
	menu_button.add_popup_item("关闭插件", self, "close_plugin")


##  关闭插件
func close_plugin():
	# 返回当前脚本所在文件夹，这个文件夹名就是插件名
	var plugin_name = get_current_plugin_name()
	# 插件名与目录名相同
	get_editor_interface().set_plugin_enabled(plugin_name, false)


##  重新加载插件
func reload_plugin():
	
	# 需要对 当前接口 存入到变量
	# 否则关闭插件yield后就无法获取了
	var interface = get_editor_interface()
	var plugin_name = get_current_plugin_name()
	
	print("-".repeat(50))
	print("[%s] 重新加载插件 " % plugin_name)
	
	# 移除 menu_button
	_remove_menu_button()
	
	# 关闭插件
	get_editor_interface().set_plugin_enabled(plugin_name, false)
	
	# 打开插件
	interface.set_plugin_enabled(plugin_name, true)
	print("[%s] 插件已重新加载" % plugin_name)
	print("-".repeat(50))


##  移除 menu_button 菜单按钮
func _remove_menu_button() -> void:
	# [释放 menu_button]
	# 必须要通过以下这种方式获取 Source 节点，
	# 因为 menu_button 存入变量中的方式
	# 会在修改时失效，造成移除失败
	var interface = get_editor_interface()
	
	# 获取编辑器视图
	var v_editor = interface.get_editor_viewport()
	# 代码编辑器菜单的父节点
	var menu_button_parent = v_editor.get_child(2).get_child(0).get_child(0)
	# 寻找添加的 menu_button 节点，并 queue_free
	var b : Node
	for n in menu_button_parent.get_children():
		if n.name.find(get_button_name()) != -1:
			b = n
			break
	if is_instance_valid(b):
		b.queue_free()


##  返回插件名
func get_current_plugin_name():
	return class_manager.util_script.get_script_dir_name(self.get_script())

