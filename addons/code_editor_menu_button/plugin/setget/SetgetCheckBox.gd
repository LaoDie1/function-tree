tool
extends HBoxContainer


onready var m_name = $Name
onready var n_set = $Set
onready var n_get = $Get



#==================================================
#   Set/Get
#==================================================
func set_property(value: String):
	if not self.is_inside_tree():
		yield(self, "ready")
	m_name.text = value

func is_selected_setter():
	return n_set.pressed && not n_set.disabled

func is_selected_getter():
	return n_get.pressed && not n_get.disabled

func set_setter_disabled(value: bool) -> void:
	if not self.is_inside_tree():
		yield(self, "ready")
	n_set.disabled = value
	if value:
		n_set.pressed = true

func set_getter_disabled(value: bool) -> void:
	if not self.is_inside_tree():
		yield(self, "ready")
	n_get.disabled = value
	if value:
		n_get.pressed = true

##  设置全选状态 
## @value  是否两个按钮都选择
func set_select_all(value: bool) -> void:
	set_setter_pressed(value)
	set_getter_pressed(value)


func set_setter_pressed(value: bool) -> void:
	if not n_set.disabled:
		n_set.pressed = value

func set_getter_pressed(value: bool) -> void:
	if not n_get.disabled:
		n_get.pressed = value


#==================================================
#   连接信号
#==================================================
func _on_Name_pressed() -> void:
	if n_set.pressed != n_get.pressed:
		if not n_set.disabled:
			n_set.pressed = true
		if not n_get.disabled:
			n_get.pressed = true
	else:
		if not n_set.disabled:
			n_set.pressed = !n_set.pressed
		if not n_get.disabled:
			n_get.pressed = !n_get.pressed

