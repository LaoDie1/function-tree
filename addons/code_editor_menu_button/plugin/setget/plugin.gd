extends "../plugin_base.gd"


const SetgetDialog = preload("SetgetDialog.tscn")


var class_manager : ClassManager_
var parse_shortcut : ClassManager_.UtilParseShortcut

var setget_dialog = SetgetDialog.instance()



#==================================================
#   内置方法
#==================================================
func _init(infc: EditorInterface):
	# 解析快捷键
	class_manager = ClassManager_.new(infc)
	parse_shortcut = ClassManager_.UtilParseShortcut.new()
	
	# SetgetDialog 窗口节点
	setget_dialog.set_interface(infc)
	infc.get_editor_viewport().add_child(setget_dialog)



#==================================================
#   自定义方法
#==================================================
#(override)
func init_menu_button(menu_button: NodeMenuButtonPlus):
	var idx = 0
	# 菜单调用的方法
	idx = menu_button.add_popup_item("生成 Setget 方法", self, "show_dialog")
	# 设置菜单快捷键
	menu_button.set_item_shortcut(idx, parse_shortcut.parse_shortcut("Ctrl + Alt + Shift + G"))


##  重写 SetGet 方法
func show_dialog():
	setget_dialog.popup_centered()
	setget_dialog.textedit = class_manager.util_script_editor.get_current_code_textedit()
	setget_dialog.update_list()



