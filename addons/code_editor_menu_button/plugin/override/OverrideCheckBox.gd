tool
extends HBoxContainer


const TypeToString = preload("TypeToString.gd")


var interface : EditorInterface
var data setget set_data


onready var check_box = $CheckBox
onready var keyword_label = $KeyWord
onready var function_label = $Function
onready var type_label = $Type



#==================================================
#   Set/Get
#==================================================
func is_pressed() -> bool:
	return check_box.pressed


func set_data(value):
	data = value
	if data == null:
		return
	
	if function_label == null:
		yield(self, "ready")
	
	# 方法名
	function_label.text = data['name']
	
	# 参数
	var args = ""
	for i in data['args'].size():
		args += "_arg" + str(i) + ", "
	args = args.trim_suffix(", ")
	function_label.text += "(" + args + "): "
	
	# 返回类型
	var return_type = TypeToString.get_type_string(data['return']['type'])
	type_label.text = return_type



#==================================================
#   内置方法
#==================================================
func _ready():
	var se = interface.get_editor_settings()
	
	var keyword_color : Color = se.get_setting("text_editor/highlighting" + "/keyword_color")
	keyword_label.modulate = keyword_color
	
	var base_type_color : Color = se.get_setting("text_editor/highlighting" + "/base_type_color")
	type_label.modulate = base_type_color
	
	self.connect("gui_input", self, "_on_click")


func _on_click(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT && event.pressed:
			check_box.pressed = !check_box.pressed
