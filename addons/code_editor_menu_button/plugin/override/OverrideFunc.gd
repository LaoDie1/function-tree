#==================================================
#	Override Func
#==================================================
# 重写方法
#==================================================
# @path: res://addons/code_editor_menu_button/plugin/override/OverrideFunc.gd
# @datetime: 2021-7-4 22:21:13
#==================================================
tool
extends WindowDialog


const OverrideCheckBox = preload("OverrideCheckBox.tscn")
const TypeToString = preload("TypeToString.gd")


signal ok(data_list)
signal cancel()


onready var custom_method = find_node("Custom")


var interface : EditorInterface



#==================================================
#   自定义方法
#==================================================
##  展示方法列表
## @method_list  方法数据列表
func show_method_list(method_list: Array) -> void:
	# 清除所有子节点
	for child in custom_method.get_children():
		child.queue_free()
	
	# 方法数据排序，通过方法名排序
	var _temp_data = {}
	for data in method_list:
		# 防止添加重复的方法
		if _temp_data.has(data["name"]):
			continue
		_temp_data[data['name']] = data
	var m_name_list = _temp_data.keys()
	m_name_list.sort()	# 排序
	
	# 添加 CheckBox 节点
	var data = {}
	for method in m_name_list:
		data = _temp_data[method]
		var checkbox = OverrideCheckBox.instance()
		checkbox.interface = interface
		checkbox.set_data(data)
		custom_method.call_deferred("add_child", checkbox)



#==================================================
#   连接信号
#==================================================
func _on_OK_pressed():
	# 获取勾选的 CheckBox 的数据
	var data_list = []
	for child in custom_method.get_children():
		if child.is_pressed():
			data_list.push_back(child.data)
	emit_signal("ok", data_list)
	self.hide()


func _on_Cancel_pressed():
	self.hide()
	emit_signal("cancel")
