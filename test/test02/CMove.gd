extends "res://addons/function_tree/src/custom/CustomFunction.gd"


#(override)
func _process_input(arg0):
	# 获取用户输入（下面四个是小键盘上的按键，按下进行控制）
	var vel = Input.get_vector("ui_left","ui_right","ui_up","ui_down")
	# 获取 Move 功能节点，操控其移动
	get_function("Move").control(vel)

