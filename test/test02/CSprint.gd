extends "res://addons/function_tree/src/custom/CustomFunction.gd"



#(override)
func _process_input(arg0):
	if Input.is_action_pressed("click"):
		# 获取鼠标的全局位置
		var mouse_pos = (host as Node2D).get_global_mouse_position()
		# 操作 Sprint 方法，冲刺到鼠标位置
		get_function("Sprint").control(mouse_pos)

