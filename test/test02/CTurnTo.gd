extends "res://addons/function_tree/src/custom/CustomFunction.gd"


#(override)
func _process_input(arg0):
	# 获取鼠标的全局位置
	var mouse_pos = (host as Node2D).get_global_mouse_position()
	# 旋转到鼠标位置
	get_function("TurnTo").control(mouse_pos)
