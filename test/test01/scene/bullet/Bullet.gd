extends Area2D


var velocity = Vector2(350, 0)


func _physics_process(delta):
	velocity.y += gravity * delta
	position += velocity * delta
	rotation = velocity.angle()


##  爆炸
func explode():
	queue_free()


func _on_Area2D_body_entered(_body: Node):
#	print_debug(_body, "造成伤害")
	explode()

