## Player
extends KinematicBody2D


signal took_damage(data)
signal dead


export var health : float = 5
export var health_max : float = 5
export var move_speed : int = 300
export var damage : float = 1.0
export (float, 0, 1) var gravity : float = 0.65
export var max_gravity : int = 2000
export var jump_height : int = 800


onready var root = $Root


#==================================================
#   内部方法
#==================================================
func _ready():
	# 设置玩家的属性
	root.get_property().move_speed = move_speed
	root.get_property().gravity = gravity
	root.get_property().max_gravity = max_gravity
	root.get_property().jump_height = jump_height



#==================================================
#   自定义方法
#==================================================
func take_damage(data) -> void:
	health -= data.damage
	emit_signal("took_damage", data)
	if health <= 0:
		emit_signal("dead")


func _on_Aniamtions_play(anim_name):
	$Label.text = anim_name
	pass # Replace with function body.

