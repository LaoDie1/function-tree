extends "res://addons/function_tree/src/custom/CustomFunction.gd"


export var left : String = "ui_left"
export var right : String = "ui_right"
export var up : String = "ui_up"
export var down : String = "ui_down"


var move


#(override)
func _init_node():
	move = get_function("Move")


#(override)
func _process_finish(_arg0):
	var vel = Input.get_vector(left, right, up, down)
	if vel != Vector2.ZERO:
		move.control(vel)



