extends "res://addons/function_tree/src/custom/CustomFunction.gd"


const State = "attack_state"


## 动画播放器
export var anim_player : NodePath
onready var _anim_player := get_node(anim_player) as AnimationPlayer

## 命中区域
export var hit_box : NodePath
onready var _hit_box := get_node(hit_box) as Area2D



#==================================================
#   Set/Get
#==================================================
##  获取动画长度
func get_anim_length(anim_name: String) -> float:
	var anim := _anim_player.get_animation(anim_name)
	return anim.length



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_data():
	._init_data()
	get_blackboard().CustomData[State] = true
	set_collision_disabled(true)


#(override)
func _process_execute(arg0):
	var n = 0
	if Input.is_key_pressed(KEY_1):
		n = 1
	elif Input.is_key_pressed(KEY_2):
		n = 2
	elif Input.is_key_pressed(KEY_3):
		n = 3
	
	# 按下了上面的几个数字键，并且 CustomData[State] 值为 true
	# 则进行执行下面的内容
	if (n && get_blackboard().CustomData[State]):
		# 设置播放的动画
		var anim_name := 'attack0' + str(n)
		get_events().get_node("PlayAnimation").play_animation = anim_name
		get_function("Attack").after_shake = get_anim_length(anim_name)
		# 执行功能
		get_skills().control("Attack", null)


##  设置碰撞体 Disabled 属性
## @value  
func set_collision_disabled(value: bool):
	for child in _hit_box.get_children():
		if (child is CollisionShape2D
			|| child is CollisionPolygon2D
		):
			child.set_deferred("disabled", value)



#==================================================
#   信号连接方法
#==================================================
func _on_Attack_ready_execute():
	# 准备攻击的时候设置攻击状态为 false
	get_blackboard().CustomData[State] = false

func _on_Attack_executed():
	# 命中区域生效
	set_collision_disabled(false)

func _on_Attack_execute_finished():
	# 命中区域失效
	set_collision_disabled(true)

func _on_Attack_status_refreshed():
	# 攻击状态刷新了，设置攻击状态为 true
	get_blackboard().CustomData[State] = true


