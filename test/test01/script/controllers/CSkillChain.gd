extends "res://addons/function_tree/src/custom/CustomFunction.gd"


const SkillChain = preload("res://addons/function_tree/src/base/function/actions/SkillChain.gd")


## 使用间隔时间
export var interval : float = 1.0

var skill_chain : SkillChain

var _enabled := true



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_node():
	._init_node()
	skill_chain = get_function("SkillChain")


#(override)
func _process_input(_arg0):
	if (
		_enabled
		&& Input.is_action_pressed("click")
		&& skill_chain.is_can_control()
	):
		_enabled = false
		# 链式启动（先执行 Attack，然后执行 Teleporting）
		skill_chain \
			.chain("Attack") \
			.chain("Teleporting", host.get_global_mouse_position()) \
			.control()
		print('----')



#==================================================
#   连接信号
#==================================================
func _on_SkillChain_all_finished():
	if interval > 0:
		yield(get_tree().create_timer(interval), "timeout")
	_enabled = true

