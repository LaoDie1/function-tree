extends "res://addons/function_tree/src/custom/CustomFunction.gd"



## 进行跳跃的 InputMap 映射 Key
export var jump_key : String = "ui_up"


#(override)
func _process_input(arg0):
	._process_input(arg0)
	if Input.is_action_pressed(jump_key):
		get_actions().control("MultistageJump", null)
