extends "res://addons/function_tree/src/custom/CustomFunction.gd"


# 选择一个 2d 节点，在移动方向发生改变时节点进行镜像翻转
export var node2d : NodePath
onready var _node2d = get_node(node2d) as Node2D



#(override)
func _process_finish(arg0):
	._process_finish(arg0)
	if (get_property().moved_velocity.x		# 上次移动的距离
		&& get_property().last_velocity.x	# 上次是否进行移动了
	):
		# 节点缩放进行翻转
		_node2d.scale.x = abs(_node2d.scale.x) * -sign(get_property().last_velocity.x)

