extends "res://addons/function_tree/src/base/function/events/ByMoveEvent.gd"


const Anim = {
	RUN = "run",
	IDLE = "default",
	JUMP = "jump",
}

# 选择一个 RayCast2D 判断是否在地面上
export var on_floor : NodePath
onready var _on_floor := get_node(on_floor) as RayCast2D



#==================================================
#   自定义方法
#==================================================
#(override)
func _start_move():
	if host.is_on_floor():
		get_parent().play_anima(Anim.RUN)

#(override)
func _stop_move():
	if host.is_on_floor():
		get_parent().play_anima(Anim.IDLE)

#(override)
func _fly_up():
	get_parent().play_anima(Anim.JUMP)

#(override)
func _fall_down():
	if !_on_floor.is_colliding():
		get_parent().play_anima(Anim.JUMP)

#(override)
func _fall_on_floor():
	if property.moved_velocity.x:
		get_parent().play_anima(Anim.RUN)
	else:
		get_parent().play_anima(Anim.IDLE)

#(override)
func _touch_ceil():
	property.velocity.y = 0

