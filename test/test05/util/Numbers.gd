tool
extends HBoxContainer


const Number = preload("Number.gd")


enum ScaleMode {
	STRETCH_KEEP_CENTERED = 3,
	STRETCH_KEEP_ASPECT,
	STRETCH_KEEP_ASPECT_CENTERED,
	STRETCH_KEEP_ASPECT_COVERED,
}


export (ScaleMode) var scale_mode : int = ScaleMode.STRETCH_KEEP_CENTERED setget set_scale_mode 
export var number : int = 0 setget set_number
export var min_size : Vector2 = Vector2(64, 64) setget set_min_size



func set_scale_mode(value: int) -> void:
	scale_mode = value
	for child in get_children():
		child.stretch_mode = scale_mode


func set_number(value: int) -> void:
	var last = str(number).length()	# 上次数字个数
	var curr = str(value).length()	# 当前数字个数
	
	if curr > last:
		for i in range(curr, last):
			add_child(Number.new())
	elif curr < last:
		for i in range(get_child_count()-1, curr+1, -1):
			get_child(i).queue_free()
	
	if get_child_count() < curr:
		for i in range(get_child_count(), curr):
			add_child(Number.new())
	elif get_child_count() > curr:
		for i in range(curr, get_child_count()):
			get_child(i).queue_free()
	
	for child in get_children():
		if child is Number:
			child.set_number(int(str(value).substr(child.get_index(), 1)))
			child.rect_min_size = min_size
			child.expand = true
			child.stretch_mode = TextureRect.STRETCH_KEEP_CENTERED
	
	number = value


func set_min_size(value: Vector2) -> void:
	min_size = value
	for child in get_children():
		child.rect_min_size = min_size


func _ready():
	set_number(number)

