tool
extends TextureRect


const ResNumber = preload("Numbers.tres")

export (int, 0, 9) var number : int = 0 setget set_number 

var _texture : AtlasTexture


func set_number(value: int) -> void:
	number = value
	if !_texture:
		_texture = ResNumber.duplicate()
	_texture.region.position.x = value * 64
	texture = _texture


func _ready():
	set_number(number)
