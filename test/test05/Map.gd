extends Node2D


export var width : int = 20
export var height : int = 100


onready var map = $TileMap


#==================================================
#   Set/Get
#==================================================
func set_col(col: int , start: int, end: int):
	if start > end:
		var temp = end
		end = start
		start = temp
	for i in range(start, end+1):
		map.set_cell(col, i, 0)

func set_row(row: int, start: int, end: int):
	if start > end:
		var temp = end
		end = start
		start = temp
	for i in range(start, end+1):
		map.set_cell(i, row, 0)


#==================================================
#   内部方法
#==================================================
func _ready():
	pass
	
	var thickness : int = 5
	# 列
	for i in range(0, thickness):
		set_col(-i, -thickness+1, height+thickness-1)
		set_col(width+i, -thickness+1, height+thickness-1)
	# 行
	for i in range(0, thickness):
		set_row(-i, -thickness+1, width+thickness-1)
		set_row(height+i, -thickness+1, width+thickness-1)
	
	# 生成两边
	both_sides()



#==================================================
#   自定义方法
#==================================================
# 两边添加
func both_sides():
	var data = {}
	for row in height:
		var gene = randf() 
		if gene < 0.33:
			var left_right = randf()
			var length = randi() % int(width * 0.5)	# 最大宽度比0.8
			if left_right < 0.5:
				data[row] = [0, length]
			else:
				data[row] = [width, width - length]
	
	# 距离不能太近
	var last_row = 0
	for row in data.keys():
		if row - last_row <= 3:
			data.erase(row)
		else:
			last_row = row
	
	# 生成
	for row in data:
		set_row(row, data[row][0], data[row][1])
	


