extends Camera2D


export var speed = 50


func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_WHEEL_DOWN:
			position.y += speed
		elif event.button_index == BUTTON_WHEEL_UP:
			position.y -= speed
