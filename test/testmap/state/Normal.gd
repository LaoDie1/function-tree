#==================================================
#	Normal
#==================================================
# @datetime: 2021-12-27 18:27:20
#==================================================
extends "res://addons/function_tree/src/base/function/statemachineroot/State.gd"


export var left : String = "ui_left"
export var right : String = "ui_right"


var componse_input
var move 
var play_anim

var _can_exe : bool = true



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_node():
	._init_node()
	
	move = get_function("Move")
	
	# 添加组合键
	componse_input = get_function("ComponseInput")
	componse_input.add_mapper(
		GCK.Anim.Chop
		, [GCK.Inputs.Attack,GCK.Inputs.Jump]
	)
	componse_input.add_mapper(
		GCK.Anim.FlyingKick
		, [GCK.Inputs.Kick, GCK.Inputs.Left, GCK.Inputs.Right]
		, []
		, [GCK.Inputs.Left, GCK.Inputs.Right]
	)
	componse_input.connect("released", self, "_released_key")
	
	play_anim = get_function("PlayAnim")



#==================================================
#   状态方法
#==================================================
#(override)
func enter(_arg0):
	_can_exe = true
	update_state()


#(override)
func exit():
	_can_exe = false


var key_map = {
	KEY_1 : GCK.Anim.LightningStrike,
	KEY_2 : GCK.Anim.Brandish,
	KEY_3 : GCK.Anim.Chop,
	KEY_4 : GCK.Anim.EarthBreakingSword,
}

#(override)
func _process_input(_arg0):
	if _can_exe:
		var dir = Input.get_action_strength(right) - Input.get_action_strength(left)
		if dir != 0:
			move.control(Vector2(dir, 0))
#		elif Input.is_action_pressed("cast"):
#			execute(GCK.Anim.LightningStrike)
		
		for key in key_map:
			if Input.is_key_pressed(key):
				execute(key_map[key])
				break


#(override)
func state_process(delta: float):
	update_state()



#==================================================
#   功能方法
#==================================================
func update_state():
	if host.is_on_floor() || property.velocity.y == 0:
		play_anim.play(
			GCK.Anim.Run \
				if property.velocity.x != 0 \
				else \
			GCK.Anim.Idle)
	else:
		play_anim.play(GCK.Anim.Jump)


##  ComponseInput 释放掉的 Key 
## @keys  释放掉的 key 值列表
## @release_all  是否全部释放掉了，全部释放掉则是被使用了
## @map_name  对应映射的名称
func _released_key(keys: Array, release_all: bool, _map_name: String):
	if not _can_exe:
		return
	
	# 在此对不同的组合按键做判断
	# 然后决定进行播放哪种行为
	
	if not release_all:
		# 如果释放中的包含 jump，则跳跃
		if keys.has(GCK.Inputs.Jump):
			execute(GCK.Anim.Jump)
		
		elif keys.has(GCK.Inputs.Attack):
			# 随机播放其中一个
			execute(GCK.Anim.Attack)
		
		elif keys.has(GCK.Inputs.Kick):
			execute(GCK.Anim.Kick)
	
	else:
		match _map_name:
			GCK.Anim.Chop:
				if host.is_on_floor():
					execute(GCK.Anim.Chop)
			
			GCK.Anim.FlyingKick:
				if get_function("FlyingKick").is_enabled():
					var dir = property.face_dir.x
					execute(GCK.Anim.FlyingKick, Vector2(dir * 800, -400))
				elif host.is_on_floor():
					execute(GCK.Anim.Kick)


##  执行
## @action_name  动作名
func execute(action_name: String, data = null):
	var _data = {
		action_name = action_name,
		data = data
	}
	if action_name in [
		GCK.Anim.Attack,
		GCK.Anim.Kick,
	]:
		switch_to("Attack", _data)
	
	elif action_name == GCK.Anim.Jump:
		get_function("Jump").control()
	
	else:
		switch_to("Skill", _data)
	


