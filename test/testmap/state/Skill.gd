#==================================================
#	Skill
#==================================================
# @datetime: 2021-12-28 17:11:45
#==================================================
extends "res://addons/function_tree/src/base/function/statemachineroot/State.gd"


## 数据
var ActionData = {
	GCK.Anim.FlyingKick : {duration = 1.0, },
	GCK.Anim.LightningStrike : {duration = 1.0, },
}

var current_skill
var play_anim

var action_name : String



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_node():
	._init_node()
	play_anim = get_function("PlayAnim")



#==================================================
#   状态方法
#==================================================
#(override)
func enter(data):
	action_name = data.action_name
	
	# 如果技能不能使用，则返回到 normal
	if not get_function(action_name).is_enabled():
		switch_to("Normal")
		return
	
	current_skill = get_function(action_name)
	
	# 设置对应技能属性
	if not ActionData.has(action_name):
		
		if action_name == GCK.Anim.EarthBreakingSword:
			action_name = GCK.Anim.LightningStrike
		var time = play_anim.get_anim_length(action_name)
		if "duration" in current_skill:
			current_skill.duration = time
		else:
			current_skill.before_shake = 0.1
			current_skill.after_shake = time - 0.1
	
	# 上面有设置数据，则开始设置
	else:
		var d = ActionData[action_name]
		for property in d:
			if property in current_skill:
				current_skill.set(property, d[property])
			else:
				print_debug(current_skill, "中没有[ ", property, " ]属性！")
	
	if action_name == GCK.Anim.EarthBreakingSword:
		action_name = GCK.Anim.LightningStrike
	
	# 播放动画
	play_anim.play(action_name)
	# 调用技能
	current_skill.control(data.data)
	
#	if action_name == GCK.Anim.FlyingKick:
#		get_custom_data()[GCK.E.Gravity] = false


#(override)
func exit():
	current_skill.stop()
#	if action_name == GCK.Anim.FlyingKick:
#		get_custom_data()[GCK.E.Gravity] = true



#==================================================
#   连接信号
#==================================================
func _on_SkillSignalHandle_action_executed(_action_name):
	# 防止其他动作同样调用这个方法
	if current_state_is_self():
		switch_to("Normal")

