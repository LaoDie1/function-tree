extends "res://addons/function_tree/src/custom/CustomFunction.gd"


export var anim_sprite : NodePath
onready var _anim_sprite := get_node(anim_sprite) as AnimatedSprite

var current_animation : String = ""


##  获取动画长度
## @anim_name  
## @return  
func get_anim_length(anim_name: String) -> float:
	var frames := _anim_sprite.frames as SpriteFrames
	var count = frames.get_frame_count(anim_name)	# 帧数
	var speed = frames.get_animation_speed(anim_name)	# 帧速率
	var scale = _anim_sprite.speed_scale	# 速度缩放 
	if count == 0:
		return 0.5
	return count * 1.0 / speed * scale


#(override)
func _init_data():
	._init_data()
	register_to_function("PlayAnim")


##  播放动画
## @anim_name  
func play(anim_name: String):
	if current_animation != anim_name:
		_anim_sprite.play(anim_name)
