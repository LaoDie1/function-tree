extends "res://addons/function_tree/src/custom/CustomFunction.gd"


export var sprite : NodePath
onready var _sprite := get_node(sprite) as Node2D


#(override)
func _process_finish(_arg0):
	if property.moved_velocity.x != 0:
		_sprite.scale.x = -property.face_dir.x
