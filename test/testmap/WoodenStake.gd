extends KinematicBody2D

onready var tween = $Tween
onready var sprite = $Icon


func take_damage(data):
	tween.stop_all()
	
	var init_value : Vector2 = sprite.material.get("shader_param/deformation")
	var final_value = init_value
	final_value.x = randf()
	tween.interpolate_property(
		sprite.material, "shader_param/deformation",
		init_value, final_value, 0.5
	)
	tween.start()
	yield(tween, "tween_all_completed")
	
	final_value.x *= -1
	final_value.x += randf() * 0.3
	tween.interpolate_property(
		sprite.material, "shader_param/deformation",
		sprite.material.get("shader_param/deformation"), 
		final_value, 
		1
	)
	tween.start()
	yield(tween, "tween_all_completed")
	
	tween.interpolate_property(
		sprite.material, "shader_param/deformation",
		sprite.material.get("shader_param/deformation"), 
		Vector2(0,0), 
		0.5
	)
	tween.start()

