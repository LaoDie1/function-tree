extends "res://addons/function_tree/src/base/Condition.gd"


#(override)
func condition() -> bool:
	return host.is_on_floor()
