extends "res://addons/function_tree/src/custom/CustomFunction.gd"


const LightningStrike = preload("res://test/test04/assets/lightning_strike/LightningStrike.tscn")

export var radius : int = 300

var e : bool = false
var t = 0.0


#(override)
func _process_execute(delta):
	if e:
		t -= delta
		if t <= 0.0:
			t = rand_range(0.15, 0.45)
			var x = rand_range(-radius, radius)
			var pos = host.global_position
			pos.x += x
			
			var l = LightningStrike.instance()
			l.global_position = pos
			get_tree().current_scene.add_child(l)


func _on_LightningStrike_execute_duration():
	e = true


func _on_LightningStrike_execute_finished():
	e = false
