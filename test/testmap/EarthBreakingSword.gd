extends "res://addons/function_tree/src/custom/CustomFunction.gd"

const EBS = preload("res://test/test04/assets/earth breaking sword/EarthBreakingSword.tscn")


func _on_EarthBreakingSword_executed():
	var pos = host.global_position
	var dir = property.face_dir.x
	var count = 3
	for i in count:
		var e = EBS.instance()
		e.global_position = pos
		get_tree().current_scene.add_child(e)
		e.global_position.x = pos.x + dir * (i+1) * 60
		if i < count - 1:
			yield(get_tree().create_timer(0.33), "timeout")

