#==================================================
#	Skill Signal Handle
#==================================================
#  技能信号处理
#==================================================
# @datetime: 2021-12-28 16:39:47
#==================================================
extends "res://addons/function_tree/src/custom/SkillDataHandle.gd"


signal action_executed(action_name)


#(override)
func _init_data():
	._init_data()
	# 添加响应的技能
	Data.execute_finished = {}
	for skill in get_skills().get_children():
		Data.execute_finished[skill.name] = ""
	# 去掉普通攻击动作
	Data.execute_finished.erase("Attack")
	Data.execute_finished.erase("Kick")
	
	Data.stopped = Data.execute_finished
	Data.duration_finished = Data.execute_finished


#(override)
func handle(skill: Node, signal_name: String, data):
	emit_signal("action_executed", skill.name)

