#==================================================
#	GCK - Global Custom Keys
#==================================================
#  全局 Key 值
#==================================================
# @datetime: 2021-12-27 18:38:59
#==================================================

class_name GCK
extends Reference


const E = {
	EControl = "Control",
	Gravity = "Gravity",
}


const Anim = {
	Idle = "Default",
	Jump = "Jump",
	Run = "Run",
	Kick = "Kick",
	Attack = "Attack",
	Attack01 = "Attack01",
	Attack02 = "Attack02",
	SideKick = "SideKick",	# 侧踢
	Chop = "Chop",
	TakeDamage = "TakeDamage",
	DrawSword = "DrawSword",
	ElegantAttack = "ElegantAttack",
	JustinGatlin = "JustinGatlin",
	FlyingKick = "FlyingKick",
	Brandish = "Brandish",	# 挥剑
	Puncture = "Puncture",	# 穿刺
	LightningStrike = "LightningStrike",	# 雷击
	EarthBreakingSword = "EarthBreakingSword",	# 破土剑
}

## 按键映射
const Inputs = {
	Up = "ui_up",
	Left = "ui_left",
	Right = "ui_right",
	Down = "ui_down",
	
	Jump = "ui_up",
	Attack = "attack",
	Kick = "kick",
}
