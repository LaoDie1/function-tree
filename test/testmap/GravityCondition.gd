extends "res://addons/function_tree/src/base/Condition.gd"



#(override)
func _init_data():
	._init_data()
	get_custom_data()[GCK.E.Gravity] = true


#(override)
func condition() -> bool:
	return get_custom_data()[GCK.E.Gravity]


