#==================================================
#	Execute Action
#==================================================
#  执行动作
#==================================================
# @datetime: 2021-12-26 17:09:09
#==================================================
extends "res://addons/function_tree/src/custom/CustomFunction.gd"


# 变招计数
var change_attack = ChangeAttackCounter.new(self)


#==================================================
#   自定义方法
#==================================================
#(override)
func _init_data():
	._init_data()
	register_to_function("ExecuteAction")
	
	# 每次只能执行一个动作
	get_custom_data()[GCK.E.EControl] = true


#(override)
func control(data):
	if not get_custom_data()[GCK.E.EControl]:
		return
	
	var disabled : bool = true
	match data.action_name:
		# [ 攻击 ]
		PlayerKeys.Animations.Attack01 \
		, PlayerKeys.Animations.Attack02 \
		:
			disabled = _attack(data.action_name)
		
		# [ 踢腿 ]
		PlayerKeys.Animations.Kick \
		, PlayerKeys.Animations.Chop \
		:
			disabled = _kick(data.action_name)
		
		# [ 跳跃 ]
		PlayerKeys.Animations.Jump:
			# 执行功能
			get_function("Jump").control()
			# 跳跃不用禁用
			disabled = false
		
		# [ 飞踢 ]
		PlayerKeys.Animations.FlyingKick:
			# 执行功能
			disabled = get_function("FlyingKick").control(
				data.data
			)
		
		# [ 受伤 ]
		PlayerKeys.Animations.TakeDamage:
			# 停止所有自定义技能
			for child in get_layer("CustomSkills").get_children():
				child.stop()
			disabled = get_function("TakeDamage").control(null)
		
		# 不符合任何情况，则 return
		_:
			return
	
	# 清空所有组合键输入
	get_function("ComponseInput").clear_all()
	
	if disabled:
		# 设置“禁用播放动画”的时间长度为动画的长度
		# 用于禁止操控再执行其他技能
		get_custom_data()[PlayerKeys.CustomDataKeys.EnabledControl] = false
		var length : float = get_function("Animations").get_anim_length(data.action_name)
		yield(get_tree().create_timer(length), "timeout")
		get_custom_data()[PlayerKeys.CustomDataKeys.EnabledControl] = true


##  攻击动作
## @anim_name  
## @return  返回是否执行成功
func _attack(anim_name: String) -> bool:
	change_attack.start(0.8)
	
	# 连续攻击4次，则播放拔剑攻击
	if change_attack.next() > 4:
		change_attack.stop()
		anim_name = "draw_sword"
	
	# 设置播放的动画
	get_custom_data()[PlayerKeys.CustomDataKeys.Attack] = anim_name
	return get_function("Attack").control(null)


##  踢腿
## @anim_name  
## @return  返回执行是否成功
func _kick(anim_name: String) -> bool:
	get_custom_data()[PlayerKeys.CustomDataKeys.Kick] = anim_name
	return get_function("Kick").control(null)



#==================================================
#   变招计数器
#==================================================
class ChangeAttackCounter:
	var attack_count : int = 0
	var change_attack_timer : Timer = Timer.new()
	
	func _init(host: Node):
		change_attack_timer.one_shot = true
		change_attack_timer.autostart = false
		if change_attack_timer.connect("timeout", self, "stop") != OK:
			print_debug("连接出现问题")
		if not host.is_inside_tree():
			yield(host, "tree_entered")
		host.add_child(change_attack_timer)
	
	func get_count() -> int:
		return attack_count
	
	func next() -> int:
		attack_count += 1
		return attack_count
	
	func start(time: float = INF):
		if time == INF:
			time = 1.0	# 默认 1.0
		change_attack_timer.start(time)
	
	func stop():
		# 重置 
		change_attack_timer.stop()
		attack_count = 0
	

