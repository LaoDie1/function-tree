extends "res://addons/function_tree/src/custom/CustomFunction.gd"


export var pos : NodePath


const Ware = preload("res://test/test04/assets/ware/Ware.tscn")


func _on_Brandish_executed():
	yield(get_tree().create_timer(0.2), "timeout")
	var ware = Ware.instance()
	get_tree().current_scene.add_child(ware)
	ware.global_position = get_node(pos).global_position
	ware.rotation = property.face_dir.angle() + PI

