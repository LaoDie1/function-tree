#==================================================
#	Patrol
#==================================================
#  巡逻
#==================================================
# @datetime: 2021-12-29 18:25:12
#==================================================
extends "res://addons/function_tree/src/base/function/statemachineroot/State.gd"


export var min_move_distance : float = 150
export var max_move_distance : float = 300


var move_to
var monitoring

var traget_pos : Vector2



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_node():
	._init_node()
	move_to = get_function("MoveTo")
	monitoring = get_function("Monitoring")


#(override)
func enter(pos = null):
	if not pos is Vector2:
		# 向前移动
		pos = host.global_position as Vector2
		pos.x += property.face_dir.x * rand_range(min_move_distance, max_move_distance)
	traget_pos = pos
	if monitoring.is_cliff_edge():
		change_move_dir()
	else:
		# 移动到目标位置
		move_to.control(traget_pos)


#(override)
func exit():
	move_to.stop()


#(override)
func state_process(_arg0):
	if monitoring.get_attack_target():
		switch_to("Attack")
	elif monitoring.is_cliff_edge():
		change_move_dir()
	elif abs(host.global_position.x - traget_pos.x) <= 30:
		switch_to("Idle")


##  改变移动方向
func change_move_dir():
	# 剩余距离
	var dist : float = abs(host.global_position.x - traget_pos.x)
	# 计算反向位置
	var pos := host.global_position as Vector2
	pos.x += dist * -property.face_dir.x
	move_to.control(pos)




