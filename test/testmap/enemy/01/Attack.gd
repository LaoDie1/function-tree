#==================================================
#	Attack
#==================================================
#  攻击
#==================================================
# @datetime: 2021-12-29 18:31:10
#==================================================
extends "res://addons/function_tree/src/base/function/statemachineroot/State.gd"


var monitoring
var move_to


#(override)
func enter(_arg0):
	monitoring = get_function("Monitoring")
	move_to = get_function("MoveTo")


#(override)
func exit():
	move_to.stop()


#(override)
func state_process(_arg0):
	if monitoring.get_attack_target() == null:
		switch_to("Idle")


