#==================================================
#	Idle
#==================================================
#  闲置
#==================================================
# @datetime: 2021-12-29 18:25:18
#==================================================
extends "res://addons/function_tree/src/base/function/statemachineroot/State.gd"


var timer : Timer
var monitoring


#(override)
func enter(time = null):
	if is_instance_valid(timer):
		timer = Timer.new()
		timer.one_shot = true
		timer.autostart = false
		timer.connect("timeout", self, "switch_to", ["Patrol"])
		add_child(timer)
	if typeof(time) in [TYPE_REAL, TYPE_INT]:
		timer.start(time)
	else:
		timer.start(rand_range(1, 2))
	
	monitoring = get_function("Monitoring")


#(override)
func exit():
	timer.stop()


#(override)
func state_process(_arg0):
	.state_process(_arg0)
	if monitoring.get_attack_target():
		switch_to("Attack")



