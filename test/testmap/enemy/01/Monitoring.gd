#==================================================
#	Monitoring
#==================================================
#  监测环境
#==================================================
# @datetime: 2021-12-29 18:26:25
#==================================================

extends "res://addons/function_tree/src/custom/CustomFunction.gd"


#(override)
func _init_data():
	._init_data()
	register_to_function("Monitoring")


##  获取攻击目标
func get_attack_target():
	return null


##  到达悬崖边
func is_cliff_edge():
	return false


