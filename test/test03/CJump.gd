extends "res://addons/function_tree/src/custom/CustomFunction.gd"


#(override)
func _process_input(arg0):
	if (Input.is_action_pressed("ui_up")
		&& host.is_on_floor()
	):
		get_function("Jump").control()
