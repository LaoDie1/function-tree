extends KinematicBody2D


export var move_speed : int = 300
export (float, 0, 1.0) var gravity : float = 0.7
export var max_gravity : int = 2000
export var jump_height : int = 700


onready var root = $FunctionRoot


func _ready():
	root.property.move_speed = move_speed
	root.property.gravity = gravity
	root.property.max_gravity = max_gravity
	root.property.jump_height = jump_height

