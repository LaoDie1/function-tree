extends "res://addons/function_tree/src/custom/CustomFunction.gd"


#(override)
func _process_input(arg0):
	var d = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if d:
		get_function("Move").control(Vector2(d, 0))
