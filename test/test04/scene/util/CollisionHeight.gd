## 检测碰撞是否超过高度
tool
extends Node2D


export var max_height : int = -20 setget set_max_height
export (int, LAYERS_2D_PHYSICS) var collision_mask : int = 1 setget set_collision_mask

onready var up = $Up
onready var down = $Down


#==================================================
#   Set/Get
#==================================================
func set_max_height(value: int) -> void:
	max_height = value
	if has_node("Up"):
		$Up.position.y = value


func set_collision_mask(value: int) -> void:
	collision_mask = value
	if has_node("Up"):
		$Up.collision_mask = value
		$Down.collision_mask = value

##  地面高度是低的
## @return  
func is_low() -> bool:
	# 两帧都是碰撞的
	return down.is_colliding() && !up.is_colliding()



#==================================================
#   内部方法
#==================================================
func _ready():
	set_max_height(max_height)
	up.collision_mask = collision_mask
	down.collision_mask = collision_mask




