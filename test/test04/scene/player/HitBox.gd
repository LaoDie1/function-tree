#==================================================
#	Hit Box
#==================================================
#  打击盒，发出伤害
#==================================================
# @datetime: 2021-12-25 17:59:45
#==================================================
extends Area2D


export var damage : float = 1



#==================================================
#   内部方法
#==================================================
func _ready():
	if not is_connected("area_entered", self, "_on_HitBox_area_entered"):
		if connect("area_entered", self, "_on_HitBox_area_entered") != OK:
			print(self, "连接时出现错误")



#==================================================
#   连接信号
#==================================================
func _on_HitBox_area_entered(area: Area2D):
	if area.has_method("take_damage"):
		# 伤害到的目标
		var damage_ = DamageData.new()
		damage_.host = owner
		damage_.damage = owner.damage
		damage_.target = area
		area.take_damage(damage_)



#==================================================
#   类
#==================================================
## 伤害数据
class DamageData extends Reference:
	var host : Node
	var damage : float
	var target : Node
