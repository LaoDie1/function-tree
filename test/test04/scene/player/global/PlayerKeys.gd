#==================================================
#	Player Keys
#==================================================
#  Player 的自定义 Key 值名称
#==================================================
# @datetime: 2021-12-24 01:45:23
#==================================================

class_name PlayerKeys
extends Node


## 动画名（也可以叫动作名）
const Animations = {
	Idle = "default",
	Jump = "jump",
	Run = "run",
	Kick = "kick",
	Attack01 = "attack01",
	Attack02 = "attack02",
	Chop = "chop",
	TakeDamage = "take_damage",
	DrawSword = "draw_sword",
	ElegantAttack = "elegant_attack",
	JustinGatlin = "justin_gatlin",
	Brandish = "brandish",	# 挥剑
	Puncture = "puncture",	# 穿刺
}

## 按键映射
const InputMaps = {
	Up = "ui_up",
	Left = "ui_left",
	Right = "ui_right",
	Down = "ui_down",
	
	Jump = "ui_up",
	Attack = "attack",
	Kick = "kick",
}

## 自定义数据
const CustomDataKeys = {
	Attack = "attack_anima",
	Kick = "kick_anima",
	UseSkill = "use_skill",
	EnabledControl = "enabled_control", 
}


