extends "res://addons/function_tree/src/discard/Animations.gd"


## 动画播放器
export var anim_player : NodePath
onready var _anim_player := get_node(anim_player) as AnimationPlayer


#==================================================
#   Set/Get
#==================================================
##  获取动画长度
func get_anim_length(anim_name: String) -> float:
	var anim := _anim_player.get_animation(anim_name)
	return anim.length
