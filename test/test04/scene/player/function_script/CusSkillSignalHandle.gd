#==================================================
#	Cus Skill Signal Handle
#==================================================
#  自定义技能信号连接
#==================================================
# @datetime: 2021-12-24 13:50:52
#==================================================

extends "res://addons/function_tree/src/custom/SkillDataHandle.gd"


#(override)
func _init_data():
	._init_data()
	
	# 连接自定义技能的信号
	for skill in get_parent() \
			.get_parent() \
			.get_node("CustomSkills") \
			.get_children():
		connect_skill_signal(skill)
	
