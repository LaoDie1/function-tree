#==================================================
#	Move Smooth
#==================================================
#  平滑移动
# * 如果前方障碍物比较小，则不会被碰撞阻挡
# * 注意射线位置，如果射线尾部传过碰撞体，则检测不到
#    射线碰撞体在父节点之上，如果碰到了也会检测成
#    父节点之上的碰撞体
# * 注意当前示例选择的射线的 collision_mask 属性
#    和其他场景中碰撞节点的属性
#==================================================
# @path: res://test/test01/script/enhance/MoveSmooth.gd
# @datetime: 2021-12-16 15:44:52
#==================================================
extends "res://addons/function_tree/src/custom/CustomFunction.gd"


# 靠上一点的射线（有障碍时判断这个射线是否有碰撞
# 没有碰撞则代表可以移动上去）
export var up : NodePath
onready var _up_ray = get_node(up) as RayCast2D
# 底部的射线（判断脚前方是否有障碍）
export var down : NodePath
onready var _down_ray = get_node(down) as RayCast2D


# 持续帧数（时间短则有卡顿，时间长则看起来假）
var t : float = 0.0


onready var collision_height = owner.get_node("Detect/CollisionHeight")


#(override)
func _process_execute(delta):
	if host.is_on_floor():
		if collision_height && collision_height.is_low():
			# 防止左右转向的时候跳一下的问题
			yield(get_tree(), "physics_frame")
			# 持续 5 帧是防止快速下落
			t = delta * 5
	
	# 上升
	if t > 0:
		if !collision_height.is_low():
			t -= delta
			if property.velocity.x:
				host.move_and_slide(Vector2(0, -property.move_speed))
		else:
			t = 0


