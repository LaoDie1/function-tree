#==================================================
#	C Input
#==================================================
#  
#==================================================
# @datetime: 2021-12-24 17:36:04
#==================================================
extends "res://addons/function_tree/src/custom/CustomFunction.gd"


var play : bool = true


#==================================================
#   自定义方法
#==================================================

##  攻击（播放攻击动画）
## @anim_name  
func attack(anim_name):
	# 设置播放的动画
	get_custom_data()[PlayerKeys.CustomDataKeys.Attack] = anim_name
	# 执行功能
	get_function("Attack").control(null)



##  ComponseInput 释放掉的 Key 
## @keys  释放掉的 key 值列表
## @release_all  是否全部释放掉了，全部释放掉则是被使用了
## @map_name  对应映射的名称
func _released_key(keys: Array, release_all: bool, _map_name: String):
	if !play:
		return
	
	if not release_all:
		# 如果释放中的包含 jump，则跳跃
		if keys.has("ui_up"):
			get_function("Jump").control()
		elif keys.has("attack"):
			attack("attack01")
		elif keys.has("kick"):
			attack("kick")
	else:
		match _map_name:
			"chop":
				attack("chop")
			"flying_kick":
				get_function("FlyingKick").control(
					Vector2(800 * property.face_dir.x, -400)
				)
			"attack02":
				attack("attack02")
		# 清空所有组合键输入
		get_function("ComponseInput").clear_all()
		play = false
		yield(get_tree().create_timer(0.1), "timeout")
		play = true
