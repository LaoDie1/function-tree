## Flip - 图像翻转
extends "res://addons/function_tree/src/custom/CustomFunction.gd"


# 选择一个 2d 节点，在移动方向发生改变时节点进行镜像翻转
export var node2d : NodePath
onready var _node2d := get_node(node2d) as Node2D


#(override)
func _init_data():
	property.face_dir = Vector2.LEFT


#(override)
func _process_finish(_arg0):
	# 节点缩放进行翻转
#	if not host.is_in_group("Player"):
#		print(property.face_dir.x)
	_node2d.scale.x = abs(_node2d.scale.x) * -property.face_dir.x


