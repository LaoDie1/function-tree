## 眩晕
extends "res://addons/function_tree/src/base/function/skills/Duration.gd"


#(override)
## @time  眩晕时间
func control(time: float) -> bool:
	if .control(time):
		set_duration(time)
		return true
	return false
