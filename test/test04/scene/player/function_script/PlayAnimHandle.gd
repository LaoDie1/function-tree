#==================================================
#	Play Anim Handle
#==================================================
#  播放动画
#==================================================
# @datetime: 2021-12-23 20:38:48
#==================================================
extends "res://test/test04/scene/player/function_script/CusSkillSignalHandle.gd"


#==================================================
#   Set/Get
#==================================================
##  获取动画长度
func get_anim_length(anim_name: String) -> float:
	return get_function("Animations").get_anim_length(anim_name)



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_data():
	._init_data()
	
	Data.ready_execute = {
		Attack = "",	# 这个动画在下方动态改变
		Kick = "",	# 这个动画在下方动态改变
		TakeDamage = PlayerKeys.Animations.TakeDamage,
	}
	
	var d = {
		Attack = "",
		Kick = "",
		FlyingKick = "", 	# 在下方动态改变
		TakeDamage = "",
	}
	Data.execute_finished = d
	Data.stopped = d


#(override)
func handle(skill: Node, _signal_name: String, data):
	# 等待时间，避免与移动或其他动作切换时造成的动画冲突，导致的没有播放动画
	yield(get_tree().create_timer(0.06), "timeout")
	
	# Attack 节点发出信号时做如下处理
	if _signal_name == "ready_execute":
		if skill.name in ["Attack", "Kick"]:
			# 获取当前自定义数据中的播放的动画
			if skill.name == "Attack":
				data = get_custom_data()[PlayerKeys.CustomDataKeys.Attack]
			elif skill.name == "Kick":
				data = get_custom_data()[PlayerKeys.CustomDataKeys.Kick]
			
			# 设置后摇时间
			skill.before_shake = 0.1
			skill.after_shake = get_anim_length(data) - skill.before_shake
			skill.interval = skill.before_shake + skill.after_shake 
			# 移动向量改为 0
			property.velocity.x = 0
	
	elif _signal_name in ['execute_finished', 'stopped']:
		reset(data)
	
	# 播放动画
	get_function("Animations").play_anima(data)


##  重设动画
## @anim_name  
func reset(anim_name):
	if host.is_on_floor() || abs(property.moved_velocity.y) <= 1:
		anim_name = (PlayerKeys.Animations.Idle if property.velocity.x == 0 else PlayerKeys.Animations.Run)
	else:
		anim_name = PlayerKeys.Animations.Jump
	get_function("Animations").play_anima(anim_name)


