#==================================================
#	Enabled State
#==================================================
# @datetime: 2021-12-22 23:23:20
#==================================================
extends "res://test/test04/scene/player/function_script/CusSkillSignalHandle.gd"



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_data():
	._init_data()
	
	Data.ready_execute = {
		FlyingKick = {move=false, jump=false, gravity=false, },
	}
	# 技能执行完成和停止都是处理相同的数据内容
	var finish_stop = {
		FlyingKick = {move=true, jump=true, gravity=true, },
	}
	Data.execute_finished = finish_stop
	Data.stopped = finish_stop


#(override)
func handle(_skill: Node, _signal_name: String, data: Dictionary) -> void:
	# 修改 enabled 属性
	pass
#	for _property in data.keys():
#		if _property in enabled:
#			enabled.get(_property).set_value(data[_property])

