#==================================================
#	C Comp Input
#==================================================
#  控制组合键输入
#==================================================
# @datetime: 2021-12-24 17:36:19
#==================================================

extends "res://addons/function_tree/src/custom/CustomFunction.gd"


var execute_action 



#(override)
func is_enabled() -> bool:
	return get_custom_data()[PlayerKeys.CustomDataKeys.EnabledControl]


#(override)
func _init_data():
	._init_data()
	get_custom_data()[PlayerKeys.CustomDataKeys.EnabledControl] = true
	
	# 添加组合键
	var componse_input = get_function("ComponseInput")
	componse_input.add_mapper("chop"
		, [PlayerKeys.InputMaps.Attack,PlayerKeys.InputMaps.Jump]
	)
	componse_input.add_mapper("flying_kick"
		, [PlayerKeys.InputMaps.Kick, PlayerKeys.InputMaps.Left, PlayerKeys.InputMaps.Right]
		, []
		, [PlayerKeys.InputMaps.Left, PlayerKeys.InputMaps.Right]
	)


#(override)
func _init_node():
	._init_node()
	execute_action = get_function("ExecuteAction")


#(override)
func _process_input(_arg0):
	if is_enabled():
		if Input.is_action_pressed("ttt"):
			execute(PlayerKeys.Animations.TakeDamage)
		elif Input.is_action_pressed("cast"):
			execute(PlayerKeys.Animations.Brandish)


##  执行
## @anim_name  
func execute(anim_name: String):
	execute_action.control(anim_name)


##  ComponseInput 释放掉的 Key 
## @keys  释放掉的 key 值列表
## @release_all  是否全部释放掉了，全部释放掉则是被使用了
## @map_name  对应映射的名称
func _released_key(keys: Array, release_all: bool, _map_name: String):
	if not is_enabled():
		return false
	
	# 在此对不同的组合按键做判断
	# 然后决定进行播放哪种行为
	
	if not release_all:
		# 如果释放中的包含 jump，则跳跃
		if keys.has(PlayerKeys.InputMaps.Jump):
			execute(PlayerKeys.Animations.Jump)
		
		elif keys.has(PlayerKeys.InputMaps.Attack):
			var list = [
				PlayerKeys.Animations.Attack01, 
				PlayerKeys.Animations.Attack02,
			]
			# 随机播放其中一个
			execute(list[randi() % list.size()])
		
		elif keys.has(PlayerKeys.InputMaps.Kick):
			execute(PlayerKeys.InputMaps.Kick)
	
	else:
		match _map_name:
			PlayerKeys.Animations.Chop:
				if host.is_on_floor():
					execute(PlayerKeys.Animations.Chop)
			
			PlayerKeys.Animations.FlyingKick:
				if get_function("FlyingKick").is_enabled():
					execute(PlayerKeys.Animations.FlyingKick)
				elif host.is_on_floor():
					execute(PlayerKeys.Animations.Kick)


