#==================================================
#	Flying Kick
#==================================================
# 飞踢
#==================================================
# @datetime: 2021-12-29 09:40:15
#==================================================

extends "res://addons/function_tree/src/base/function/skills/Duration.gd"


export (float, 0.0, 1.0) var gravity : float = 1.0
## 坠落速度
export var gravity_speed : int = 1200


## 初始速度
var init_velocity : Vector2
## 速度插值权重
var weight : float = 1.0
# 用于累计最小持续时间
var t = 0.0



#==================================================
#   自定义方法
#==================================================
#(override)
func control(_init_velocity: Vector2):
	if is_enabled() && .control(_init_velocity):
		property.velocity.y = 0
		t = 0
		weight = 1.0 / duration
		init_velocity = _init_velocity
		host.move_and_slide(init_velocity, Vector2.UP)
		return true
	return false


#(override)
func _process_duration(delta):
	t += delta
	if (
		!host.is_on_floor() && !host.is_on_wall()
		&& abs(init_velocity.x) > 1
		|| t < 0.2
	):
#		init_velocity.y += gravity_speed * delta 
		init_velocity.y = lerp(init_velocity.y, 0, gravity * delta)
		init_velocity.x = lerp(init_velocity.x, 0, weight * delta)
		host.move_and_slide(init_velocity, Vector2.UP)
	else:
		init_velocity.y = 0
		# 结束持续
		interrupt()


