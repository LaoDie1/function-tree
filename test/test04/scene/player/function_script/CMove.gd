#==================================================
#	C Move
#==================================================
# @datetime: 2021-12-25 15:16:11
#==================================================

extends "res://addons/function_tree/src/custom/CustomFunction.gd"


export var left : String = "ui_left"
export var right : String = "ui_right"


var move


#(override)
func _init_node():
	move = get_function("Move")


#(override)
func _process_finish(_arg0):
	if get_custom_data()[PlayerKeys.CustomDataKeys.EnabledControl]:
		var dir = Input.get_action_strength(right) - Input.get_action_strength(left)
		if dir != 0:
			move.control(Vector2(dir, 0))
	

