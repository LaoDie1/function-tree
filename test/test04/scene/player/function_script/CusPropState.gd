#==================================================
#	Custom Property State - 自定义属性状态
#==================================================
# @datetime: 2021-12-22 23:20:39
#==================================================
extends "res://test/test04/scene/player/function_script/CusSkillSignalHandle.gd"



#==================================================
#   Set/Get
#==================================================
##  设置自定义数据
func set_custom_data(key: String, value):
	get_blackboard().CustomData[key] = value



#==================================================
#   自定义方法
#==================================================
#(override)
func _init_data():
	._init_data()
	# 设置数据(设置技能刷新时的数据的内容)
	for skill in Data.status_refreshed:
		var data = Data.status_refreshed[skill]
		for key in data:
			set_custom_data(key, data[key])
	set_custom_data(PlayerKeys.CustomDataKeys.UseSkill, true)
	set_custom_data(PlayerKeys.CustomDataKeys.EnabledControl, true)


#(override)
func handle(skill: Node, signal_name: String, data):
	# 所有技能，只要是下方判断的信号，则就执行
	if signal_name == "ready_execute":
		set_custom_data(PlayerKeys.CustomDataKeys.UseSkill, false)
		set_custom_data(PlayerKeys.CustomDataKeys.EnabledControl, false)
	elif signal_name in ["execute_finished", "duration_finished"]:
		set_custom_data(PlayerKeys.CustomDataKeys.UseSkill, true)
		set_custom_data(PlayerKeys.CustomDataKeys.EnabledControl, true)
	

