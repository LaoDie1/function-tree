#==================================================
#	Execute Action
#==================================================
#  执行动作
#==================================================
# @datetime: 2021-12-26 17:09:09
#==================================================
extends "res://addons/function_tree/src/custom/CustomFunction.gd"


var attack_count : int = 0
var attack_timer : Timer = Timer.new()


#(override)
func _init_data():
	._init_data()
	register_to_function("ExecuteAction")
	
	attack_timer.one_shot = true
	attack_timer.autostart = false
	attack_timer.connect("timeout", self, "_reset_attack_count")
	add_child(attack_timer)


#(override)
func control(anim_name: String):
	if not get_custom_data()[PlayerKeys.CustomDataKeys.EnabledControl]:
		return
	
	var disabled : bool = true
	match anim_name:
		# [ 攻击 ]
		PlayerKeys.Animations.Attack01 \
		, PlayerKeys.Animations.Attack02 \
		:
			disabled = _attack(anim_name)
		
		# [ 踢腿 ]
		PlayerKeys.Animations.Kick \
		, PlayerKeys.Animations.Chop \
		:
			disabled = _kick(anim_name)
		
		# [ 跳跃 ]
		PlayerKeys.Animations.Jump:
			# 执行功能
			get_function("Jump").control()
			# 跳跃不用禁用
			disabled = false
		
		# [ 飞踢 ]
		PlayerKeys.Animations.FlyingKick:
			# 执行功能
			disabled = get_function("FlyingKick").control(
				Vector2(800 * property.face_dir.x, -400)
			)
		
		# [ 受伤 ]
		PlayerKeys.Animations.TakeDamage:
			# 停止所有自定义技能
			for child in get_layer("CustomSkills").get_children():
				child.stop()
			disabled = get_function("TakeDamage").control(null)
		
		# 不符合任何情况，则 return
		_:
			return
	
	# 清空所有组合键输入
	get_function("ComponseInput").clear_all()
	
	if disabled:
		# 设置“禁用播放动画”的时间长度为动画的长度
		# 用于禁止操控再执行其他技能
		get_custom_data()[PlayerKeys.CustomDataKeys.EnabledControl] = false
		var length : float = get_function("Animations").get_anim_length(anim_name)
		yield(get_tree().create_timer(length), "timeout")
		get_custom_data()[PlayerKeys.CustomDataKeys.EnabledControl] = true


##  攻击动作
## @anim_name  
## @return  返回是否执行成功
func _attack(anim_name: String) -> bool:
	# 连续攻击4次，则播放拔剑攻击
	attack_count += 1
	if attack_count > 4:
		anim_name = "draw_sword"
		attack_timer.stop()
		attack_count = 0
	
	attack_timer.start(0.8)
	# 设置播放的动画
	get_custom_data()[PlayerKeys.CustomDataKeys.Attack] = anim_name
	return get_function("Attack").control(null)


##  踢腿
## @anim_name  
## @return  返回执行是否成功
func _kick(anim_name: String) -> bool:
	get_custom_data()[PlayerKeys.CustomDataKeys.Kick] = anim_name
	return get_function("Kick").control(null)


##  重置攻击次数 
func _reset_attack_count():
	attack_count = 0
	

