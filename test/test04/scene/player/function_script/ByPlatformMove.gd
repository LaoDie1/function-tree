#==================================================
#	By Platform Move
#==================================================
# 根据平台移动播放动画
#==================================================
# @datetime: 2021-12-22 14:44:05
#==================================================
extends "res://addons/function_tree/src/base/function/events/ByMoveEvent.gd"


const Anim = {
	IDLE = "default",
	RUN = "run",
	JUMP = "jump",
}

export var on_floor : NodePath
onready var _on_floor := get_node(on_floor) as RayCast2D



#==================================================
#   自定义方法
#==================================================
#(override)
func _start_move():
	if (_on_floor && _on_floor.is_colliding() 
		|| host.is_on_floor()
	):
		get_parent().play_anima(Anim.RUN)
	else:
		# 不在地板上
		if (get_parent() \
			.get_current_animation() in [Anim.IDLE, Anim.RUN]
		):
			get_parent().play_anima(Anim.JUMP)

#(override)
func _stop_move():
	if host.is_on_floor():
		get_parent().play_anima(Anim.IDLE)

#(override)
func _fly_up():
	get_parent().play_anima(Anim.JUMP)

#(override)
func _fall_down():
	get_parent().play_anima(Anim.JUMP)

#(override)
func _fall_on_floor():
	if property.moved_velocity.x:
		get_parent().play_anima(Anim.RUN)
	else:
		get_parent().play_anima(Anim.IDLE)

#(override)
func _touch_ceil():
	# 碰到天花板
	property.velocity.y = 0
	

