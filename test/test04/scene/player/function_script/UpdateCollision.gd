#==================================================
#	Update Collision
#==================================================
#  更新碰撞
#==================================================
# @datetime: 2021-12-23 22:23:06
#==================================================
extends "res://test/test04/scene/player/function_script/CusSkillSignalHandle.gd"


## 命中区域
export var hit_box : NodePath
onready var _hit_box := get_node(hit_box) as Area2D


#==================================================
#   Set/Get
#==================================================
##  设置碰撞体 Disabled 属性
## @value  设置是否关闭 bool 值
func set_collision_disabled(value: bool):
	for child in _hit_box.get_children():
		if (child is CollisionShape2D
			|| child is CollisionPolygon2D
		):
			child.set_deferred("disabled", value)


#==================================================
#   自定义方法
#==================================================
#(override)
func _init_data():
	._init_data()
	
	Data.ready_execute = {
		Attack = false,
	}
	Data.execute_finished = {
		Attack = true,
	}


#(override)
func handle(_skill: Node, _signal_name: String, data):
	set_collision_disabled(data)



