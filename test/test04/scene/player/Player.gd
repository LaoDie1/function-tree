## Player
extends KinematicBody2D


signal health_changed(old_value, new_value)
signal took_damage(data)
signal dead


export var health : float = 5 setget set_health
export var health_max : float = 5
export var move_speed : int = 300
export var damage : float = 1.0
export (float, 0, 1) var gravity : float = 0.65
export var max_gravity : int = 2000
export var jump_height : int = 800


onready var root = $Root
onready var infc_control = $Root/Custom/Controllers/CCompInput



#==================================================
#   Set/Get
#==================================================
func set_health(value: float) -> void:
	emit_signal("health_changed", health, value)
	health = value



#==================================================
#   内部方法
#==================================================
func _ready():
	# 设置玩家的属性
	root.get_property().move_speed = move_speed
	root.get_property().gravity = gravity
	root.get_property().max_gravity = max_gravity
	root.get_property().jump_height = jump_height



#==================================================
#   自定义方法
#==================================================
##  受到伤害
## @data  数据
func take_damage(data) -> void:
	health -= data.damage
	emit_signal("took_damage", data)
	if health <= 0:
		emit_signal("dead")
	else:
		infc_control.play(PlayerKeys.Animations.TakeDamage)


##  施放技能
## @skill_name  技能名称
## @data  传入的数据
func cast_skill(skill_name: String, data):
	root.blackboard.get_function(skill_name).control(data)



#==================================================
#   连接信号
#==================================================
func _on_Aniamtions_play(anim_name):
	$Label.text = anim_name


