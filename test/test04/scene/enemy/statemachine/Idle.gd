#==================================================
#	Idle
#==================================================
# 闲置
#==================================================
# @datetime: 2021-12-22 14:00:11
#==================================================
extends "res://addons/function_tree/src/base/function/statemachineroot/State.gd"


var timer = Timer.new()


#(override)
func _init_data():
	._init_data()
	timer.one_shot = true
	timer.autostart = false
	# 到时间切换到 Patrol
	timer.connect("timeout", self, "switch_to", ["Patrol"])
	add_child(timer)

#(override)
func enter(_arg0):
	var time = rand_range(1, 3)
	timer.one_shot = true
	timer.start(time)
#	get_animations().play_anima("default")

#(override)
func exit():
	timer.stop()

#(override)
func state_process(_arg0):
	# 存在攻击目标
	if get_custom_data()["AttackTarget"]:
		switch_to("Attack", get_custom_data()["AttackTarget"])


