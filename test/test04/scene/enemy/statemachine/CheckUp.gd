#==================================================
#	Check Up
#==================================================
# 检查丢失攻击目标的位置
#==================================================
# @datetime: 2021-12-22 13:58:15
#==================================================
extends "res://addons/function_tree/src/base/function/statemachineroot/State.gd"


## 到达时的判定距离（不要太小，否则可能会在平台上掉下去）
export var arrive_distance : float = 50.0

var target_pos : Vector2 = Vector2(0,0)
var running = false


#(override)
func enter(pos: Vector2):
	running = true
	target_pos = pos
	get_function("MoveTo").control(pos)

#(override)
func exit():
	running = false
	get_function("MoveTo").stop()

#(override)
func state_process(_delta: float):
	# 到达位置
	if (running 
		&& abs(host.global_position.x - target_pos.x) <= arrive_distance
	):
		arrive()
	# 存在攻击目标
	elif get_custom_data()["AttackTarget"]:
		switch_to("Attack")
	# 没路或撞墙
	elif (not get_function("General").has_road()
		|| host.is_on_wall()
	):
		arrive()

## 到达位置
func arrive():
	running = false
	get_function("MoveTo").stop()
	var dir = sign(target_pos.x - host.global_position.x)
	get_function("Move").control(Vector2(-dir, 0))
	switch_to("Idle")
