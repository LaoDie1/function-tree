#==================================================
#	Patrol
#==================================================
# 巡逻
#==================================================
# @datetime: 2021-12-22 13:59:22
#==================================================

extends "res://addons/function_tree/src/base/function/statemachineroot/State.gd"


export var distance_min = 100
export var distance_max = 300


#(override)
func enter(_arg0):
	# 移动到下一个方向
	var pos = get_function("General").get_next_pos(
		property.face_dir, 
		rand_range(distance_min,distance_max)
	)
	switch_to("Run", pos)

