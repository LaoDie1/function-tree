#==================================================
#	Javelin Attack
#==================================================
# 标枪攻击
#==================================================
# @datetime: 2021-12-22 13:59:48
#==================================================
extends "res://addons/function_tree/src/base/function/statemachineroot/State.gd"


const Bullet = preload("res://test/test04/scene/bullet/Bullet.tscn")


export var bullet_pos : NodePath
onready var _bullet_pos := get_node(bullet_pos) as Node2D

## 攻击目标上次所在的位置
var last_pos : Vector2

onready var bullet_to_node = get_tree().current_scene as Node


#(override)
func state_process(_arg0):
	if get_custom_data()["AttackTarget"]:
		last_pos = get_custom_data()["AttackTarget"].global_position
		if get_function("Attack").control(null):
			pass
	else:
		print("攻击目标丢失，移动到目标之前位置 ", last_pos)
		switch_to("CheckUp", last_pos)


##  射击 
## @pos  位置
func shoot(pos: Vector2):
	var bullet = Bullet.instance() as Node2D
	bullet.global_position = _bullet_pos.global_position
	var speed = host.global_position.distance_to(pos) + 100
	bullet.velocity = host.global_position.direction_to(pos) * speed
	bullet_to_node.add_child(bullet)


func _on_Attack_executed():
	if get_custom_data()["AttackTarget"]:
		shoot(get_custom_data()["AttackTarget"].global_position)


