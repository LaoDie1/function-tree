extends RayCast2D


signal detect(object)


var target = null


func _physics_process(delta):
	if is_colliding() && target != get_collider():
		target = get_collider()
		emit_signal("detect", target)
	
	elif !is_colliding() && target:
		emit_signal("detect", null)
	


