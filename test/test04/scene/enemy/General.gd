## 通用功能
extends "res://addons/function_tree/src/custom/CustomFunction.gd"


onready var _detect_road = $"../../../../Collision/DetectRoad"


#(override)
func _init_data():
	register_to_function("General")


##  获取下一个位置
## @dir  移动方向
## @distance  移动距离
func get_next_pos(dir: Vector2, distance: float) -> Vector2:
	return dir * distance + host.global_position

##  前方是否有路
## @return  
func has_road() -> bool:
	return _detect_road.is_colliding()
