## 更新面向方向
extends "res://addons/function_tree/src/custom/CustomFunction.gd"



#(override)
func _init_data():
	property.face_dir = Vector2.LEFT


#(override)
func _process_finish(_arg0):
	if (property.last_velocity.x
		&& property.moved_velocity.x
	):
		property.face_dir.x = sign(property.last_velocity.x)


