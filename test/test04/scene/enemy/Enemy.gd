extends KinematicBody2D

export var health : float = 5
export var health_max : float = 5
export var move_speed : int = 300
export var damage : float = 1.0

onready var root = $Root


func _ready():
	# 设置玩家的属性
	root.get_property().move_speed = 300
	root.get_property().gravity = 0.7
	root.get_property().max_gravity = 2000
	root.get_property().jump_height = 800

func take_damage(data) -> void:
	health -= data.damage
	emit_signal("took_damage", data)
	if health <= 0:
		emit_signal("dead")


func _on_Attack_ready_execute():
#	print("攻击")
	pass # Replace with function body.
