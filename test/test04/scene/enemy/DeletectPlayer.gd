## 检测玩家功能
extends "res://addons/function_tree/src/custom/CustomFunction.gd"


export var detect_player_ray : NodePath
onready var _detect_player_ray := get_node(detect_player_ray) as RayCast2D

var target = null


#(override)
func _init_data():
	._init_data()
	register_to_function("DetectPlayer")
	get_custom_data()["AttackTarget"] = null


#(override)
func _process_input(_arg0):
	if _detect_player_ray.is_colliding():
		get_custom_data()["AttackTarget"] = _detect_player_ray.get_collider()
	else:
		get_custom_data()["AttackTarget"] = null

