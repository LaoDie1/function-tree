extends Area2D

export var move_speed : int = 500

var last_rot : float = 0.0
var velocity : Vector2 = Vector2.LEFT

onready var sprite = $AnimatedSprite
onready var timer = $Timer


func _ready():
	sprite.playing = true
	sprite.animation = "default"
	update_rot()


func _physics_process(delta):
	if last_rot != self.rotation:
		update_rot()
	position += velocity * delta


func update_rot():
	last_rot = self.rotation
	velocity = Vector2.LEFT.rotated(last_rot) * move_speed


func explode():
	set_physics_process(false)
	timer.stop()
	sprite.play("dead")
	$CollisionShape2D.set_deferred("disabled", true)
	yield(sprite, "animation_finished")
	queue_free()


func _on_AnimatedSprite_animation_finished():
	if sprite && sprite.animation == "default":
		sprite.animation = "run"


func _on_Timer_timeout():
	explode()


func _on_Ware_body_entered(body):
	explode()
