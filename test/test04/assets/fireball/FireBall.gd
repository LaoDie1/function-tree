extends Area2D


func _ready():
	$AnimatedSprite.playing = true
	$AnimatedSprite.frame = 0
	$AnimatedSprite.animation = "default"


func explode():
	$Timer.stop()
	$CollisionShape2D.set_deferred("disabled", true)
	$AnimatedSprite.play("dead")
	yield($AnimatedSprite, "animation_finished")
	queue_free()


func _on_FireBall_body_entered(body):
	explode()
