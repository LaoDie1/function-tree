extends Area2D


func _ready():
	$AnimatedSprite.playing = true
	$AnimatedSprite.animation = "default"
	$AnimatedSprite.frame = 0


##  遁入
func escape():
	$AnimatedSprite.play("escape")
	yield($AnimatedSprite, "animation_finished")
	queue_free()

